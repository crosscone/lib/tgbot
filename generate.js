var types = [];
var interfaces = [];
var methods = [];

var typeMapping = {
    'MessageId': 'MessageID',
    'Boolean': 'bool',
    'True': 'bool',
    'Int': 'int',
    'Integer': 'int',
    'Integer64': 'int64',
    'Float': 'float64',
    'String': 'string',
    'Integer or String': 'ChatID',
};

var typeRewrites = {
    'MessageId': 'MessageID',
    'LoginUrl': 'LoginURL',
    'Array of InputMediaPhoto and InputMediaVideo': 'InputMedia',
    'InputMediaAudio, InputMediaDocument, InputMediaPhoto and InputMediaVideo': 'InputMedia',
}

function methodToBuilder(str) {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

function simpleTypePostProcess(str) {
    if (typeMapping.hasOwnProperty(str)) {
        return typeMapping[str];
    }

    return str;
}

function typePostProcess(str) {
    if (typeRewrites.hasOwnProperty(str)) {
        return typeRewrites[str];
    }

    return str;
}
function underBarToUpperCamel(str) {
    return str.split('_').map(function (s) {
        if (s === 'id') {
            return 'ID';
        }

        if (s === 'url') {
            return 'URL';
        }

        if (s === 'ip') {
            return 'IP';
        }

        return s.charAt(0).toUpperCase() + s.slice(1)
    }).join('');
}

function underbarToCamel(str) {
    var res = underBarToUpperCamel(str);

    return res.charAt(0).toLowerCase() + res.slice(1);
}

function resolveType(type, description) {
    if (type.includes('InlineKeyboardMarkup') && type != 'InlineKeyboardMarkup') {
        return 'ReplyMarkup';
    }

    if (type.includes('InputFile')) {
        return 'InputFile';
    }

    if (type.includes('Float')) {
        return 'float64';
    }

    if (type.includes('Array of InputMediaPhoto and InputMediaVideo')) {
        return 'InputMedia';
    }

    if (description.includes('64 bit integer')) {
        type = 'Integer64';
    }

    if (type.includes('Array of Array of')) {
        var arrayType = /Array of Array of (.+)/.exec(type)[1];
        if (typeMapping.hasOwnProperty(arrayType)) {
            return '[][]' + typeMapping[arrayType];
        }

        return '[][]*' + typePostProcess(arrayType);
    }

    if (type.includes('Array of ')) {
        var arrayType = /Array of (.+)/.exec(type)[1];
        if (typeMapping.hasOwnProperty(arrayType)) {
            return '[]' + typeMapping[arrayType];
        }

        return '[]*' + typePostProcess(arrayType);
    }

    if (typeMapping.hasOwnProperty(type)) {
        return typeMapping[type];
    }

    return '*' + typePostProcess(type);
}

function resolveReturnType(description) {
    var returnType = /Returns Array of ([a-zA-Z]+) on success/.exec(description);
    if (returnType !== null) {
        return '[]*' + returnType[1];
    }

    var returnType = /an array of ([a-zA-Z]+)s that were sent is returned/.exec(description);
    if (returnType !== null) {
        return '[]*' + returnType[1];
    }

    var returnType = /an array of the sent ([a-zA-Z]+)s is returned/.exec(description);
    if (returnType !== null) {
        return '[]*' + returnType[1];
    }

    var returnType = /returns an Array of ([a-zA-Z]+)/.exec(description);
    if (returnType !== null) {
        return '[]*' + returnType[1];
    }

    var returnType = /rray of ([a-zA-Z]+) objects is returned/.exec(description);
    if (returnType !== null) {
        return '[]*' + returnType[1];
    }

    var returnType = /([a-zA-Z]+) with the final results is returned/.exec(description);
    if (returnType !== null) {
        return '*' + simpleTypePostProcess(returnType[1]);
    }

    var returnType = /([a-zA-Z]+) object is returned/.exec(description);
    if (returnType !== null) {
        return '*' + simpleTypePostProcess(returnType[1]);
    }

    var returnType = /([a-zA-Z]+) is returned/.exec(description);
    if (returnType !== null) {
        return '*' + simpleTypePostProcess(returnType[1]);
    }

    var returnType = /eturns[ a-z]* ([a-zA-Z]+)/.exec(description);
    if (returnType !== null) {
        return '*' + simpleTypePostProcess(returnType[1]);
    }

    console.log('Unknown: ' + description);

    return 'interface{}';
}

function handleMethod(elem) {
    var elems = elem.nextUntil('h3, h4');
    var rows = elems.filter('table').find('tbody tr');
    var name = elem.text();
    var methodDesc = elem.next('p').text();
    var returnType = resolveReturnType(methodDesc);
    var params = [];

    for (var i = 0; i < rows.length; i++) {
        var row = $(rows[i]);
        var cols = $(row.find('td'));
        var param = $(cols[0]).text();
        var type = $(cols[1]).text();
        var required = $(cols[2]).text();
        var description = $(cols[3]).text();
        type = resolveType(type, '');

        params.push({
            name: underbarToCamel(param),
            nameUpper: underBarToUpperCamel(param),
            description: description,
            json: param,
            type: type,
            required: required === 'Yes',
        });
    }

    methods.push({
        name: name,
        description: methodDesc,
        builder: methodToBuilder(name),
        params: params,
        returnType: returnType,
    });
}

function handleConcreteType(elem, rows) {
    var name = typePostProcess(elem.text());
    var typeDesc = elem.next('p').text();
    var fields = [];

    for (var i = 0; i < rows.length; i++) {
        var row = $(rows[i]);
        var cols = $(row.find('td'));
        var field = $(cols[0]).text();
        var type = $(cols[1]).text();
        var description = $(cols[2]).text();
        type = resolveType(type, description);

        fields.push({
            name: typePostProcess(underBarToUpperCamel(field)),
            json: field,
            type: type,
        });
    }

    types.push({
        name: name,
        description: typeDesc,
        fields: fields,
        interface: '',
    });
}

function handleInterface(elem, list) {
    var name = typePostProcess(elem.text());
    var iterfaceDesc = elem.next('p').text();
    var interfaceTypes = [];

    for (var i = 0; i < list.length; i++) {
        var item = $(list[i]);
        var type = item.text();

        interfaceTypes.push({
            name: typePostProcess(type),
        });
    }

    interfaces.push({
        name: name,
        description: iterfaceDesc,
        types: interfaceTypes,
    });
}

function handleType(elem) {
    var elems = elem.nextUntil('h3, h4');
    var list = elems.filter('ul').find('li');
    var rows = elems.filter('table').find('tbody tr');
    if (list.length > 0) {
        handleInterface(elem, list);
    } else {
        handleConcreteType(elem, rows);
    }
}

// Collect types, interfaces and methods

var headers = $('.anchor').parent().filter('h4');

interfaces.push({
    name: 'ReplyMarkup',
    description: 'This object represents reply markup.',
    types: [],
});

interfaces.push({
    name: 'ChatID',
    description: 'This object represents chat id.',
    types: [],
});

for (var i = 0; i < headers.length; i++) {
    var header = $(headers[i]);
    var headerText = header.text();
    if (headerText.length === 0 || headerText.includes(" ")) {
        continue;
    }

    if (headerText[0] === headerText[0].toUpperCase()) {
        handleType(header);
    } else {
        handleMethod(header);
    }
}

// Set type interfaces

for (var i = 0; i < types.length; i++) {
    var type = types[i];
    for (var j = 0; j < interfaces.length; j++) {
        var interface = interfaces[j];
        var stop = false;
        for (var subtype of interface.types) {
            if (type.name === subtype.name) {
                types[i].interface = interface.name;
                stop = true;
                break;
            }
        }

        if (stop) {
            break;
        }
    }
}

// Generate header code
// Code generated by protoc-gen-go. DO NOT EDIT.
// source: service.proto

var headerLines = [
    "// Code in this file was generated. DO NOT EDIT.\n",
    "// Source: https://core.telegram.org/bots/api\n",
    "// Generator: https://gitlab.com/crosscone/lib/tgbot\n\n",
    "package tg\n\n",
];

// Generate interfaces code

var interfaceLines = [];

for (const interface of interfaces) {
    interfaceLines.push('// ' + interface.name + ' - ' + interface.description + "\n");
    interfaceLines.push('type ' + interface.name + ' interface {' + "\n");
    interfaceLines.push('	' + interface.name + '()' + "\n");
    interfaceLines.push('}' + "\n\n");
}

// Generate types code

var typeLines = [];

for (const type of types) {
    typeLines.push('// ' + type.name + ' - ' + type.description + "\n");
    typeLines.push('type ' + type.name + ' struct {' + "\n");
    for (const field of type.fields) {
        typeLines.push('	' + field.name + ' ' + field.type + ' `json:"' + field.json + '"`' + "\n");
    }
    typeLines.push('}' + "\n\n");
}

// Generate methods code

var methodLines = [];

for (const method of methods) {
    let requiredParams = [];
    for (const param of method.params) {
        if (param.required) {
            requiredParams.push(param);
        }
    }

    // Method structure

    methodLines.push('// ' + method.builder + 'Method represents Telegram Bot API method ' + method.name + "\n");
    methodLines.push('// ' + method.description + "\n");
    methodLines.push('type ' + method.builder + 'Method struct {' + "\n");
    if (method.params.length > 0) {
        for (const param of method.params) {
            var required = (param.required ? ' required:"true"' : '');
            methodLines.push('	' + param.nameUpper + ' ' + param.type + ' `json:"' + param.json + '"' + required + '`' + "\n");
        }
    }

    methodLines.push("}\n\n");

    // Method constructor

    let funcParams = [];
    for (const param of requiredParams) {
        funcParams.push(param.name + ' ' + param.type);
    }

    methodLines.push('// ' + method.builder + ' creates new Telegram Bot API method ' + method.name + "\n");
    methodLines.push('// ' + method.description + "\n");
    methodLines.push('func ' + method.builder + '(' + funcParams.join(', ') + ') *' + method.builder + "Method {\n");
    methodLines.push('	return &' + method.builder + "Method{\n");
    for (const param of requiredParams) {
        methodLines.push('		' + param.nameUpper + ': ' + param.name + ",\n");
    }
    methodLines.push("	}\n");
    methodLines.push("}\n\n");

    // Method setters

    for (const param of method.params) {
        methodLines.push('// Set' + param.nameUpper + ' - ' + param.description + "\n");
        methodLines.push('func (m *' + method.builder + 'Method) Set' + param.nameUpper + '(value ' + param.type + ') *' + method.builder + 'Method {' + "\n");
        methodLines.push('	m.' + param.nameUpper + ' = value' + "\n");
        methodLines.push('	return m' + "\n");
        methodLines.push("}\n\n");
    }

    // Method sender

    methodLines.push("// Send is used to send request using Telegram Bot Client\n");
    methodLines.push('func (m *' + method.builder + 'Method) Send(client *Client) (res ' + method.returnType + ', err error) {' + "\n");
    methodLines.push('	return res, client.request("' + method.name + '", m, &res)' + "\n");
    methodLines.push("}\n\n");
}

var golangCode =
    headerLines.join('') +
    interfaceLines.join('') +
    typeLines.join('') +
    methodLines.join('');

console.log(golangCode);