package tg

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Listen starts listening for incoming updates
func (c *Client) Listen(method *SetWebhookMethod) (http.Handler, error) {
	_, err := GetMe().Send(c)
	if err != nil {
		return nil, err
	}

	_, err = method.Send(c)
	if err != nil {
		return nil, err
	}

	return &webhookHandler{
		client: c,
	}, nil
}

type webhookHandler struct {
	client *Client
}

func (h *webhookHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return
	}

	var update Update
	err = json.Unmarshal(body, &update)
	if err != nil {
		return
	}

	h.client.processUpdate(&update)
}
