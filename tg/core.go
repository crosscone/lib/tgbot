// Code in this file was generated. DO NOT EDIT.
// Source: https://core.telegram.org/bots/api
// Generator: https://gitlab.com/crosscone/lib/tgbot

package tg

// ReplyMarkup - This object represents reply markup.
type ReplyMarkup interface {
	ReplyMarkup()
}

// ChatID - This object represents chat id.
type ChatID interface {
	ChatID()
}

// InputMedia - This object represents the content of a media message to be sent. It should be one of
type InputMedia interface {
	InputMedia()
}

// InlineQueryResult - This object represents one result of an inline query. Telegram clients currently support results of the following 20 types:
type InlineQueryResult interface {
	InlineQueryResult()
}

// InputMessageContent - This object represents the content of a message to be sent as a result of an inline query. Telegram clients currently support the following 4 types:
type InputMessageContent interface {
	InputMessageContent()
}

// PassportElementError - This object represents an error in the Telegram Passport element which was submitted that should be resolved by the user. It should be one of:
type PassportElementError interface {
	PassportElementError()
}

// Update - This object represents an incoming update.At most one of the optional parameters can be present in any given update.
type Update struct {
	UpdateID           int                 `json:"update_id"`
	Message            *Message            `json:"message"`
	EditedMessage      *Message            `json:"edited_message"`
	ChannelPost        *Message            `json:"channel_post"`
	EditedChannelPost  *Message            `json:"edited_channel_post"`
	InlineQuery        *InlineQuery        `json:"inline_query"`
	ChosenInlineResult *ChosenInlineResult `json:"chosen_inline_result"`
	CallbackQuery      *CallbackQuery      `json:"callback_query"`
	ShippingQuery      *ShippingQuery      `json:"shipping_query"`
	PreCheckoutQuery   *PreCheckoutQuery   `json:"pre_checkout_query"`
	Poll               *Poll               `json:"poll"`
	PollAnswer         *PollAnswer         `json:"poll_answer"`
}

// WebhookInfo - Contains information about the current status of a webhook.
type WebhookInfo struct {
	URL                  string   `json:"url"`
	HasCustomCertificate bool     `json:"has_custom_certificate"`
	PendingUpdateCount   int      `json:"pending_update_count"`
	IPAddress            string   `json:"ip_address"`
	LastErrorDate        int      `json:"last_error_date"`
	LastErrorMessage     string   `json:"last_error_message"`
	MaxConnections       int      `json:"max_connections"`
	AllowedUpdates       []string `json:"allowed_updates"`
}

// User - This object represents a Telegram user or bot.
type User struct {
	ID                      int    `json:"id"`
	IsBot                   bool   `json:"is_bot"`
	FirstName               string `json:"first_name"`
	LastName                string `json:"last_name"`
	Username                string `json:"username"`
	LanguageCode            string `json:"language_code"`
	CanJoinGroups           bool   `json:"can_join_groups"`
	CanReadAllGroupMessages bool   `json:"can_read_all_group_messages"`
	SupportsInlineQueries   bool   `json:"supports_inline_queries"`
}

// Chat - This object represents a chat.
type Chat struct {
	ID               int64            `json:"id"`
	Type             string           `json:"type"`
	Title            string           `json:"title"`
	Username         string           `json:"username"`
	FirstName        string           `json:"first_name"`
	LastName         string           `json:"last_name"`
	Photo            *ChatPhoto       `json:"photo"`
	Bio              string           `json:"bio"`
	Description      string           `json:"description"`
	InviteLink       string           `json:"invite_link"`
	PinnedMessage    *Message         `json:"pinned_message"`
	Permissions      *ChatPermissions `json:"permissions"`
	SlowModeDelay    int              `json:"slow_mode_delay"`
	StickerSetName   string           `json:"sticker_set_name"`
	CanSetStickerSet bool             `json:"can_set_sticker_set"`
	LinkedChatID     int64            `json:"linked_chat_id"`
	Location         *ChatLocation    `json:"location"`
}

// Message - This object represents a message.
type Message struct {
	MessageID               int                      `json:"message_id"`
	From                    *User                    `json:"from"`
	SenderChat              *Chat                    `json:"sender_chat"`
	Date                    int                      `json:"date"`
	Chat                    *Chat                    `json:"chat"`
	ForwardFrom             *User                    `json:"forward_from"`
	ForwardFromChat         *Chat                    `json:"forward_from_chat"`
	ForwardFromMessageID    int                      `json:"forward_from_message_id"`
	ForwardSignature        string                   `json:"forward_signature"`
	ForwardSenderName       string                   `json:"forward_sender_name"`
	ForwardDate             int                      `json:"forward_date"`
	ReplyToMessage          *Message                 `json:"reply_to_message"`
	ViaBot                  *User                    `json:"via_bot"`
	EditDate                int                      `json:"edit_date"`
	MediaGroupID            string                   `json:"media_group_id"`
	AuthorSignature         string                   `json:"author_signature"`
	Text                    string                   `json:"text"`
	Entities                []*MessageEntity         `json:"entities"`
	Animation               *Animation               `json:"animation"`
	Audio                   *Audio                   `json:"audio"`
	Document                *Document                `json:"document"`
	Photo                   []*PhotoSize             `json:"photo"`
	Sticker                 *Sticker                 `json:"sticker"`
	Video                   *Video                   `json:"video"`
	VideoNote               *VideoNote               `json:"video_note"`
	Voice                   *Voice                   `json:"voice"`
	Caption                 string                   `json:"caption"`
	CaptionEntities         []*MessageEntity         `json:"caption_entities"`
	Contact                 *Contact                 `json:"contact"`
	Dice                    *Dice                    `json:"dice"`
	Game                    *Game                    `json:"game"`
	Poll                    *Poll                    `json:"poll"`
	Venue                   *Venue                   `json:"venue"`
	Location                *Location                `json:"location"`
	NewChatMembers          []*User                  `json:"new_chat_members"`
	LeftChatMember          *User                    `json:"left_chat_member"`
	NewChatTitle            string                   `json:"new_chat_title"`
	NewChatPhoto            []*PhotoSize             `json:"new_chat_photo"`
	DeleteChatPhoto         bool                     `json:"delete_chat_photo"`
	GroupChatCreated        bool                     `json:"group_chat_created"`
	SupergroupChatCreated   bool                     `json:"supergroup_chat_created"`
	ChannelChatCreated      bool                     `json:"channel_chat_created"`
	MigrateToChatID         int64                    `json:"migrate_to_chat_id"`
	MigrateFromChatID       int64                    `json:"migrate_from_chat_id"`
	PinnedMessage           *Message                 `json:"pinned_message"`
	Invoice                 *Invoice                 `json:"invoice"`
	SuccessfulPayment       *SuccessfulPayment       `json:"successful_payment"`
	ConnectedWebsite        string                   `json:"connected_website"`
	PassportData            *PassportData            `json:"passport_data"`
	ProximityAlertTriggered *ProximityAlertTriggered `json:"proximity_alert_triggered"`
	ReplyMarkup             *InlineKeyboardMarkup    `json:"reply_markup"`
}

// MessageID - This object represents a unique message identifier.
type MessageID struct {
	MessageID int `json:"message_id"`
}

// MessageEntity - This object represents one special entity in a text message. For example, hashtags, usernames, URLs, etc.
type MessageEntity struct {
	Type     string `json:"type"`
	Offset   int    `json:"offset"`
	Length   int    `json:"length"`
	URL      string `json:"url"`
	User     *User  `json:"user"`
	Language string `json:"language"`
}

// PhotoSize - This object represents one size of a photo or a file / sticker thumbnail.
type PhotoSize struct {
	FileID       string `json:"file_id"`
	FileUniqueID string `json:"file_unique_id"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	FileSize     int    `json:"file_size"`
}

// Animation - This object represents an animation file (GIF or H.264/MPEG-4 AVC video without sound).
type Animation struct {
	FileID       string     `json:"file_id"`
	FileUniqueID string     `json:"file_unique_id"`
	Width        int        `json:"width"`
	Height       int        `json:"height"`
	Duration     int        `json:"duration"`
	Thumb        *PhotoSize `json:"thumb"`
	FileName     string     `json:"file_name"`
	MimeType     string     `json:"mime_type"`
	FileSize     int        `json:"file_size"`
}

// Audio - This object represents an audio file to be treated as music by the Telegram clients.
type Audio struct {
	FileID       string     `json:"file_id"`
	FileUniqueID string     `json:"file_unique_id"`
	Duration     int        `json:"duration"`
	Performer    string     `json:"performer"`
	Title        string     `json:"title"`
	FileName     string     `json:"file_name"`
	MimeType     string     `json:"mime_type"`
	FileSize     int        `json:"file_size"`
	Thumb        *PhotoSize `json:"thumb"`
}

// Document - This object represents a general file (as opposed to photos, voice messages and audio files).
type Document struct {
	FileID       string     `json:"file_id"`
	FileUniqueID string     `json:"file_unique_id"`
	Thumb        *PhotoSize `json:"thumb"`
	FileName     string     `json:"file_name"`
	MimeType     string     `json:"mime_type"`
	FileSize     int        `json:"file_size"`
}

// Video - This object represents a video file.
type Video struct {
	FileID       string     `json:"file_id"`
	FileUniqueID string     `json:"file_unique_id"`
	Width        int        `json:"width"`
	Height       int        `json:"height"`
	Duration     int        `json:"duration"`
	Thumb        *PhotoSize `json:"thumb"`
	FileName     string     `json:"file_name"`
	MimeType     string     `json:"mime_type"`
	FileSize     int        `json:"file_size"`
}

// VideoNote - This object represents a video message (available in Telegram apps as of v.4.0).
type VideoNote struct {
	FileID       string     `json:"file_id"`
	FileUniqueID string     `json:"file_unique_id"`
	Length       int        `json:"length"`
	Duration     int        `json:"duration"`
	Thumb        *PhotoSize `json:"thumb"`
	FileSize     int        `json:"file_size"`
}

// Voice - This object represents a voice note.
type Voice struct {
	FileID       string `json:"file_id"`
	FileUniqueID string `json:"file_unique_id"`
	Duration     int    `json:"duration"`
	MimeType     string `json:"mime_type"`
	FileSize     int    `json:"file_size"`
}

// Contact - This object represents a phone contact.
type Contact struct {
	PhoneNumber string `json:"phone_number"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	UserID      int    `json:"user_id"`
	Vcard       string `json:"vcard"`
}

// Dice - This object represents an animated emoji that displays a random value.
type Dice struct {
	Emoji string `json:"emoji"`
	Value int    `json:"value"`
}

// PollOption - This object contains information about one answer option in a poll.
type PollOption struct {
	Text       string `json:"text"`
	VoterCount int    `json:"voter_count"`
}

// PollAnswer - This object represents an answer of a user in a non-anonymous poll.
type PollAnswer struct {
	PollID    string `json:"poll_id"`
	User      *User  `json:"user"`
	OptionIds []int  `json:"option_ids"`
}

// Poll - This object contains information about a poll.
type Poll struct {
	ID                    string           `json:"id"`
	Question              string           `json:"question"`
	Options               []*PollOption    `json:"options"`
	TotalVoterCount       int              `json:"total_voter_count"`
	IsClosed              bool             `json:"is_closed"`
	IsAnonymous           bool             `json:"is_anonymous"`
	Type                  string           `json:"type"`
	AllowsMultipleAnswers bool             `json:"allows_multiple_answers"`
	CorrectOptionID       int              `json:"correct_option_id"`
	Explanation           string           `json:"explanation"`
	ExplanationEntities   []*MessageEntity `json:"explanation_entities"`
	OpenPeriod            int              `json:"open_period"`
	CloseDate             int              `json:"close_date"`
}

// Location - This object represents a point on the map.
type Location struct {
	Longitude            float64 `json:"longitude"`
	Latitude             float64 `json:"latitude"`
	HorizontalAccuracy   float64 `json:"horizontal_accuracy"`
	LivePeriod           int     `json:"live_period"`
	Heading              int     `json:"heading"`
	ProximityAlertRadius int     `json:"proximity_alert_radius"`
}

// Venue - This object represents a venue.
type Venue struct {
	Location        *Location `json:"location"`
	Title           string    `json:"title"`
	Address         string    `json:"address"`
	FoursquareID    string    `json:"foursquare_id"`
	FoursquareType  string    `json:"foursquare_type"`
	GooglePlaceID   string    `json:"google_place_id"`
	GooglePlaceType string    `json:"google_place_type"`
}

// ProximityAlertTriggered - This object represents the content of a service message, sent whenever a user in the chat triggers a proximity alert set by another user.
type ProximityAlertTriggered struct {
	Traveler *User `json:"traveler"`
	Watcher  *User `json:"watcher"`
	Distance int   `json:"distance"`
}

// UserProfilePhotos - This object represent a user's profile pictures.
type UserProfilePhotos struct {
	TotalCount int            `json:"total_count"`
	Photos     [][]*PhotoSize `json:"photos"`
}

// File - This object represents a file ready to be downloaded. The file can be downloaded via the link https://api.telegram.org/file/bot<token>/<file_path>. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile.
type File struct {
	FileID       string `json:"file_id"`
	FileUniqueID string `json:"file_unique_id"`
	FileSize     int    `json:"file_size"`
	FilePath     string `json:"file_path"`
}

// ReplyKeyboardMarkup - This object represents a custom keyboard with reply options (see Introduction to bots for details and examples).
type ReplyKeyboardMarkup struct {
	Keyboard        [][]*KeyboardButton `json:"keyboard"`
	ResizeKeyboard  bool                `json:"resize_keyboard"`
	OneTimeKeyboard bool                `json:"one_time_keyboard"`
	Selective       bool                `json:"selective"`
}

// KeyboardButton - This object represents one button of the reply keyboard. For simple text buttons String can be used instead of this object to specify text of the button. Optional fields request_contact, request_location, and request_poll are mutually exclusive.
type KeyboardButton struct {
	Text            string                  `json:"text"`
	RequestContact  bool                    `json:"request_contact"`
	RequestLocation bool                    `json:"request_location"`
	RequestPoll     *KeyboardButtonPollType `json:"request_poll"`
}

// KeyboardButtonPollType - This object represents type of a poll, which is allowed to be created and sent when the corresponding button is pressed.
type KeyboardButtonPollType struct {
	Type string `json:"type"`
}

// ReplyKeyboardRemove - Upon receiving a message with this object, Telegram clients will remove the current custom keyboard and display the default letter-keyboard. By default, custom keyboards are displayed until a new keyboard is sent by a bot. An exception is made for one-time keyboards that are hidden immediately after the user presses a button (see ReplyKeyboardMarkup).
type ReplyKeyboardRemove struct {
	RemoveKeyboard bool `json:"remove_keyboard"`
	Selective      bool `json:"selective"`
}

// InlineKeyboardMarkup - This object represents an inline keyboard that appears right next to the message it belongs to.
type InlineKeyboardMarkup struct {
	InlineKeyboard [][]*InlineKeyboardButton `json:"inline_keyboard"`
}

// InlineKeyboardButton - This object represents one button of an inline keyboard. You must use exactly one of the optional fields.
type InlineKeyboardButton struct {
	Text                         string        `json:"text"`
	URL                          string        `json:"url"`
	LoginURL                     *LoginURL     `json:"login_url"`
	CallbackData                 string        `json:"callback_data"`
	SwitchInlineQuery            string        `json:"switch_inline_query"`
	SwitchInlineQueryCurrentChat string        `json:"switch_inline_query_current_chat"`
	CallbackGame                 *CallbackGame `json:"callback_game"`
	Pay                          bool          `json:"pay"`
}

// LoginURL - This object represents a parameter of the inline keyboard button used to automatically authorize a user. Serves as a great replacement for the Telegram Login Widget when the user is coming from Telegram. All the user needs to do is tap/click a button and confirm that they want to log in:
type LoginURL struct {
	URL                string `json:"url"`
	ForwardText        string `json:"forward_text"`
	BotUsername        string `json:"bot_username"`
	RequestWriteAccess bool   `json:"request_write_access"`
}

// CallbackQuery - This object represents an incoming callback query from a callback button in an inline keyboard. If the button that originated the query was attached to a message sent by the bot, the field message will be present. If the button was attached to a message sent via the bot (in inline mode), the field inline_message_id will be present. Exactly one of the fields data or game_short_name will be present.
type CallbackQuery struct {
	ID              string   `json:"id"`
	From            *User    `json:"from"`
	Message         *Message `json:"message"`
	InlineMessageID string   `json:"inline_message_id"`
	ChatInstance    string   `json:"chat_instance"`
	Data            string   `json:"data"`
	GameShortName   string   `json:"game_short_name"`
}

// ForceReply - Upon receiving a message with this object, Telegram clients will display a reply interface to the user (act as if the user has selected the bot's message and tapped 'Reply'). This can be extremely useful if you want to create user-friendly step-by-step interfaces without having to sacrifice privacy mode.
type ForceReply struct {
	ForceReply bool `json:"force_reply"`
	Selective  bool `json:"selective"`
}

// ChatPhoto - This object represents a chat photo.
type ChatPhoto struct {
	SmallFileID       string `json:"small_file_id"`
	SmallFileUniqueID string `json:"small_file_unique_id"`
	BigFileID         string `json:"big_file_id"`
	BigFileUniqueID   string `json:"big_file_unique_id"`
}

// ChatMember - This object contains information about one member of a chat.
type ChatMember struct {
	User                  *User  `json:"user"`
	Status                string `json:"status"`
	CustomTitle           string `json:"custom_title"`
	IsAnonymous           bool   `json:"is_anonymous"`
	CanBeEdited           bool   `json:"can_be_edited"`
	CanPostMessages       bool   `json:"can_post_messages"`
	CanEditMessages       bool   `json:"can_edit_messages"`
	CanDeleteMessages     bool   `json:"can_delete_messages"`
	CanRestrictMembers    bool   `json:"can_restrict_members"`
	CanPromoteMembers     bool   `json:"can_promote_members"`
	CanChangeInfo         bool   `json:"can_change_info"`
	CanInviteUsers        bool   `json:"can_invite_users"`
	CanPinMessages        bool   `json:"can_pin_messages"`
	IsMember              bool   `json:"is_member"`
	CanSendMessages       bool   `json:"can_send_messages"`
	CanSendMediaMessages  bool   `json:"can_send_media_messages"`
	CanSendPolls          bool   `json:"can_send_polls"`
	CanSendOtherMessages  bool   `json:"can_send_other_messages"`
	CanAddWebPagePreviews bool   `json:"can_add_web_page_previews"`
	UntilDate             int    `json:"until_date"`
}

// ChatPermissions - Describes actions that a non-administrator user is allowed to take in a chat.
type ChatPermissions struct {
	CanSendMessages       bool `json:"can_send_messages"`
	CanSendMediaMessages  bool `json:"can_send_media_messages"`
	CanSendPolls          bool `json:"can_send_polls"`
	CanSendOtherMessages  bool `json:"can_send_other_messages"`
	CanAddWebPagePreviews bool `json:"can_add_web_page_previews"`
	CanChangeInfo         bool `json:"can_change_info"`
	CanInviteUsers        bool `json:"can_invite_users"`
	CanPinMessages        bool `json:"can_pin_messages"`
}

// ChatLocation - Represents a location to which a chat is connected.
type ChatLocation struct {
	Location *Location `json:"location"`
	Address  string    `json:"address"`
}

// BotCommand - This object represents a bot command.
type BotCommand struct {
	Command     string `json:"command"`
	Description string `json:"description"`
}

// ResponseParameters - Contains information about why a request was unsuccessful.
type ResponseParameters struct {
	MigrateToChatID int64 `json:"migrate_to_chat_id"`
	RetryAfter      int   `json:"retry_after"`
}

// InputMediaPhoto - Represents a photo to be sent.
type InputMediaPhoto struct {
	Type            string           `json:"type"`
	Media           string           `json:"media"`
	Caption         string           `json:"caption"`
	ParseMode       string           `json:"parse_mode"`
	CaptionEntities []*MessageEntity `json:"caption_entities"`
}

// InputMediaVideo - Represents a video to be sent.
type InputMediaVideo struct {
	Type              string           `json:"type"`
	Media             string           `json:"media"`
	Thumb             InputFile        `json:"thumb"`
	Caption           string           `json:"caption"`
	ParseMode         string           `json:"parse_mode"`
	CaptionEntities   []*MessageEntity `json:"caption_entities"`
	Width             int              `json:"width"`
	Height            int              `json:"height"`
	Duration          int              `json:"duration"`
	SupportsStreaming bool             `json:"supports_streaming"`
}

// InputMediaAnimation - Represents an animation file (GIF or H.264/MPEG-4 AVC video without sound) to be sent.
type InputMediaAnimation struct {
	Type            string           `json:"type"`
	Media           string           `json:"media"`
	Thumb           InputFile        `json:"thumb"`
	Caption         string           `json:"caption"`
	ParseMode       string           `json:"parse_mode"`
	CaptionEntities []*MessageEntity `json:"caption_entities"`
	Width           int              `json:"width"`
	Height          int              `json:"height"`
	Duration        int              `json:"duration"`
}

// InputMediaAudio - Represents an audio file to be treated as music to be sent.
type InputMediaAudio struct {
	Type            string           `json:"type"`
	Media           string           `json:"media"`
	Thumb           InputFile        `json:"thumb"`
	Caption         string           `json:"caption"`
	ParseMode       string           `json:"parse_mode"`
	CaptionEntities []*MessageEntity `json:"caption_entities"`
	Duration        int              `json:"duration"`
	Performer       string           `json:"performer"`
	Title           string           `json:"title"`
}

// InputMediaDocument - Represents a general file to be sent.
type InputMediaDocument struct {
	Type                        string           `json:"type"`
	Media                       string           `json:"media"`
	Thumb                       InputFile        `json:"thumb"`
	Caption                     string           `json:"caption"`
	ParseMode                   string           `json:"parse_mode"`
	CaptionEntities             []*MessageEntity `json:"caption_entities"`
	DisableContentTypeDetection bool             `json:"disable_content_type_detection"`
}

// InputFile - This object represents the contents of a file to be uploaded. Must be posted using multipart/form-data in the usual way that files are uploaded via the browser.
type InputFile struct {
}

// Sticker - This object represents a sticker.
type Sticker struct {
	FileID       string        `json:"file_id"`
	FileUniqueID string        `json:"file_unique_id"`
	Width        int           `json:"width"`
	Height       int           `json:"height"`
	IsAnimated   bool          `json:"is_animated"`
	Thumb        *PhotoSize    `json:"thumb"`
	Emoji        string        `json:"emoji"`
	SetName      string        `json:"set_name"`
	MaskPosition *MaskPosition `json:"mask_position"`
	FileSize     int           `json:"file_size"`
}

// StickerSet - This object represents a sticker set.
type StickerSet struct {
	Name          string     `json:"name"`
	Title         string     `json:"title"`
	IsAnimated    bool       `json:"is_animated"`
	ContainsMasks bool       `json:"contains_masks"`
	Stickers      []*Sticker `json:"stickers"`
	Thumb         *PhotoSize `json:"thumb"`
}

// MaskPosition - This object describes the position on faces where a mask should be placed by default.
type MaskPosition struct {
	Point  string  `json:"point"`
	XShift float64 `json:"x_shift"`
	YShift float64 `json:"y_shift"`
	Scale  float64 `json:"scale"`
}

// InlineQuery - This object represents an incoming inline query. When the user sends an empty query, your bot could return some default or trending results.
type InlineQuery struct {
	ID       string    `json:"id"`
	From     *User     `json:"from"`
	Location *Location `json:"location"`
	Query    string    `json:"query"`
	Offset   string    `json:"offset"`
}

// InlineQueryResultArticle - Represents a link to an article or web page.
type InlineQueryResultArticle struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	Title               string                `json:"title"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	URL                 string                `json:"url"`
	HideURL             bool                  `json:"hide_url"`
	Description         string                `json:"description"`
	ThumbURL            string                `json:"thumb_url"`
	ThumbWidth          int                   `json:"thumb_width"`
	ThumbHeight         int                   `json:"thumb_height"`
}

// InlineQueryResultPhoto - Represents a link to a photo. By default, this photo will be sent by the user with optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the photo.
type InlineQueryResultPhoto struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	PhotoURL            string                `json:"photo_url"`
	ThumbURL            string                `json:"thumb_url"`
	PhotoWidth          int                   `json:"photo_width"`
	PhotoHeight         int                   `json:"photo_height"`
	Title               string                `json:"title"`
	Description         string                `json:"description"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultGif - Represents a link to an animated GIF file. By default, this animated GIF file will be sent by the user with optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the animation.
type InlineQueryResultGif struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	GifURL              string                `json:"gif_url"`
	GifWidth            int                   `json:"gif_width"`
	GifHeight           int                   `json:"gif_height"`
	GifDuration         int                   `json:"gif_duration"`
	ThumbURL            string                `json:"thumb_url"`
	ThumbMimeType       string                `json:"thumb_mime_type"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultMpeg4Gif - Represents a link to a video animation (H.264/MPEG-4 AVC video without sound). By default, this animated MPEG-4 file will be sent by the user with optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the animation.
type InlineQueryResultMpeg4Gif struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	Mpeg4URL            string                `json:"mpeg4_url"`
	Mpeg4Width          int                   `json:"mpeg4_width"`
	Mpeg4Height         int                   `json:"mpeg4_height"`
	Mpeg4Duration       int                   `json:"mpeg4_duration"`
	ThumbURL            string                `json:"thumb_url"`
	ThumbMimeType       string                `json:"thumb_mime_type"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultVideo - Represents a link to a page containing an embedded video player or a video file. By default, this video file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the video.
type InlineQueryResultVideo struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	VideoURL            string                `json:"video_url"`
	MimeType            string                `json:"mime_type"`
	ThumbURL            string                `json:"thumb_url"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	VideoWidth          int                   `json:"video_width"`
	VideoHeight         int                   `json:"video_height"`
	VideoDuration       int                   `json:"video_duration"`
	Description         string                `json:"description"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultAudio - Represents a link to an MP3 audio file. By default, this audio file will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the audio.
type InlineQueryResultAudio struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	AudioURL            string                `json:"audio_url"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	Performer           string                `json:"performer"`
	AudioDuration       int                   `json:"audio_duration"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultVoice - Represents a link to a voice recording in an .OGG container encoded with OPUS. By default, this voice recording will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the the voice message.
type InlineQueryResultVoice struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	VoiceURL            string                `json:"voice_url"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	VoiceDuration       int                   `json:"voice_duration"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultDocument - Represents a link to a file. By default, this file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the file. Currently, only .PDF and .ZIP files can be sent using this method.
type InlineQueryResultDocument struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	DocumentURL         string                `json:"document_url"`
	MimeType            string                `json:"mime_type"`
	Description         string                `json:"description"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
	ThumbURL            string                `json:"thumb_url"`
	ThumbWidth          int                   `json:"thumb_width"`
	ThumbHeight         int                   `json:"thumb_height"`
}

// InlineQueryResultLocation - Represents a location on a map. By default, the location will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the location.
type InlineQueryResultLocation struct {
	Type                 string                `json:"type"`
	ID                   string                `json:"id"`
	Latitude             float64               `json:"latitude"`
	Longitude            float64               `json:"longitude"`
	Title                string                `json:"title"`
	HorizontalAccuracy   float64               `json:"horizontal_accuracy"`
	LivePeriod           int                   `json:"live_period"`
	Heading              int                   `json:"heading"`
	ProximityAlertRadius int                   `json:"proximity_alert_radius"`
	ReplyMarkup          *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent  *InputMessageContent  `json:"input_message_content"`
	ThumbURL             string                `json:"thumb_url"`
	ThumbWidth           int                   `json:"thumb_width"`
	ThumbHeight          int                   `json:"thumb_height"`
}

// InlineQueryResultVenue - Represents a venue. By default, the venue will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the venue.
type InlineQueryResultVenue struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	Latitude            float64               `json:"latitude"`
	Longitude           float64               `json:"longitude"`
	Title               string                `json:"title"`
	Address             string                `json:"address"`
	FoursquareID        string                `json:"foursquare_id"`
	FoursquareType      string                `json:"foursquare_type"`
	GooglePlaceID       string                `json:"google_place_id"`
	GooglePlaceType     string                `json:"google_place_type"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
	ThumbURL            string                `json:"thumb_url"`
	ThumbWidth          int                   `json:"thumb_width"`
	ThumbHeight         int                   `json:"thumb_height"`
}

// InlineQueryResultContact - Represents a contact with a phone number. By default, this contact will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the contact.
type InlineQueryResultContact struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	PhoneNumber         string                `json:"phone_number"`
	FirstName           string                `json:"first_name"`
	LastName            string                `json:"last_name"`
	Vcard               string                `json:"vcard"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
	ThumbURL            string                `json:"thumb_url"`
	ThumbWidth          int                   `json:"thumb_width"`
	ThumbHeight         int                   `json:"thumb_height"`
}

// InlineQueryResultGame - Represents a Game.
type InlineQueryResultGame struct {
	Type          string                `json:"type"`
	ID            string                `json:"id"`
	GameShortName string                `json:"game_short_name"`
	ReplyMarkup   *InlineKeyboardMarkup `json:"reply_markup"`
}

// InlineQueryResultCachedPhoto - Represents a link to a photo stored on the Telegram servers. By default, this photo will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the photo.
type InlineQueryResultCachedPhoto struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	PhotoFileID         string                `json:"photo_file_id"`
	Title               string                `json:"title"`
	Description         string                `json:"description"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultCachedGif - Represents a link to an animated GIF file stored on the Telegram servers. By default, this animated GIF file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with specified content instead of the animation.
type InlineQueryResultCachedGif struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	GifFileID           string                `json:"gif_file_id"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultCachedMpeg4Gif - Represents a link to a video animation (H.264/MPEG-4 AVC video without sound) stored on the Telegram servers. By default, this animated MPEG-4 file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the animation.
type InlineQueryResultCachedMpeg4Gif struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	Mpeg4FileID         string                `json:"mpeg4_file_id"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultCachedSticker - Represents a link to a sticker stored on the Telegram servers. By default, this sticker will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the sticker.
type InlineQueryResultCachedSticker struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	StickerFileID       string                `json:"sticker_file_id"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultCachedDocument - Represents a link to a file stored on the Telegram servers. By default, this file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the file.
type InlineQueryResultCachedDocument struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	Title               string                `json:"title"`
	DocumentFileID      string                `json:"document_file_id"`
	Description         string                `json:"description"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultCachedVideo - Represents a link to a video file stored on the Telegram servers. By default, this video file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the video.
type InlineQueryResultCachedVideo struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	VideoFileID         string                `json:"video_file_id"`
	Title               string                `json:"title"`
	Description         string                `json:"description"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultCachedVoice - Represents a link to a voice message stored on the Telegram servers. By default, this voice message will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the voice message.
type InlineQueryResultCachedVoice struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	VoiceFileID         string                `json:"voice_file_id"`
	Title               string                `json:"title"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InlineQueryResultCachedAudio - Represents a link to an MP3 audio file stored on the Telegram servers. By default, this audio file will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the audio.
type InlineQueryResultCachedAudio struct {
	Type                string                `json:"type"`
	ID                  string                `json:"id"`
	AudioFileID         string                `json:"audio_file_id"`
	Caption             string                `json:"caption"`
	ParseMode           string                `json:"parse_mode"`
	CaptionEntities     []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup         *InlineKeyboardMarkup `json:"reply_markup"`
	InputMessageContent *InputMessageContent  `json:"input_message_content"`
}

// InputTextMessageContent - Represents the content of a text message to be sent as the result of an inline query.
type InputTextMessageContent struct {
	MessageText           string           `json:"message_text"`
	ParseMode             string           `json:"parse_mode"`
	Entities              []*MessageEntity `json:"entities"`
	DisableWebPagePreview bool             `json:"disable_web_page_preview"`
}

// InputLocationMessageContent - Represents the content of a location message to be sent as the result of an inline query.
type InputLocationMessageContent struct {
	Latitude             float64 `json:"latitude"`
	Longitude            float64 `json:"longitude"`
	HorizontalAccuracy   float64 `json:"horizontal_accuracy"`
	LivePeriod           int     `json:"live_period"`
	Heading              int     `json:"heading"`
	ProximityAlertRadius int     `json:"proximity_alert_radius"`
}

// InputVenueMessageContent - Represents the content of a venue message to be sent as the result of an inline query.
type InputVenueMessageContent struct {
	Latitude        float64 `json:"latitude"`
	Longitude       float64 `json:"longitude"`
	Title           string  `json:"title"`
	Address         string  `json:"address"`
	FoursquareID    string  `json:"foursquare_id"`
	FoursquareType  string  `json:"foursquare_type"`
	GooglePlaceID   string  `json:"google_place_id"`
	GooglePlaceType string  `json:"google_place_type"`
}

// InputContactMessageContent - Represents the content of a contact message to be sent as the result of an inline query.
type InputContactMessageContent struct {
	PhoneNumber string `json:"phone_number"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Vcard       string `json:"vcard"`
}

// ChosenInlineResult - Represents a result of an inline query that was chosen by the user and sent to their chat partner.
type ChosenInlineResult struct {
	ResultID        string    `json:"result_id"`
	From            *User     `json:"from"`
	Location        *Location `json:"location"`
	InlineMessageID string    `json:"inline_message_id"`
	Query           string    `json:"query"`
}

// LabeledPrice - This object represents a portion of the price for goods or services.
type LabeledPrice struct {
	Label  string `json:"label"`
	Amount int    `json:"amount"`
}

// Invoice - This object contains basic information about an invoice.
type Invoice struct {
	Title          string `json:"title"`
	Description    string `json:"description"`
	StartParameter string `json:"start_parameter"`
	Currency       string `json:"currency"`
	TotalAmount    int    `json:"total_amount"`
}

// ShippingAddress - This object represents a shipping address.
type ShippingAddress struct {
	CountryCode string `json:"country_code"`
	State       string `json:"state"`
	City        string `json:"city"`
	StreetLine1 string `json:"street_line1"`
	StreetLine2 string `json:"street_line2"`
	PostCode    string `json:"post_code"`
}

// OrderInfo - This object represents information about an order.
type OrderInfo struct {
	Name            string           `json:"name"`
	PhoneNumber     string           `json:"phone_number"`
	Email           string           `json:"email"`
	ShippingAddress *ShippingAddress `json:"shipping_address"`
}

// ShippingOption - This object represents one shipping option.
type ShippingOption struct {
	ID     string          `json:"id"`
	Title  string          `json:"title"`
	Prices []*LabeledPrice `json:"prices"`
}

// SuccessfulPayment - This object contains basic information about a successful payment.
type SuccessfulPayment struct {
	Currency                string     `json:"currency"`
	TotalAmount             int        `json:"total_amount"`
	InvoicePayload          string     `json:"invoice_payload"`
	ShippingOptionID        string     `json:"shipping_option_id"`
	OrderInfo               *OrderInfo `json:"order_info"`
	TelegramPaymentChargeID string     `json:"telegram_payment_charge_id"`
	ProviderPaymentChargeID string     `json:"provider_payment_charge_id"`
}

// ShippingQuery - This object contains information about an incoming shipping query.
type ShippingQuery struct {
	ID              string           `json:"id"`
	From            *User            `json:"from"`
	InvoicePayload  string           `json:"invoice_payload"`
	ShippingAddress *ShippingAddress `json:"shipping_address"`
}

// PreCheckoutQuery - This object contains information about an incoming pre-checkout query.
type PreCheckoutQuery struct {
	ID               string     `json:"id"`
	From             *User      `json:"from"`
	Currency         string     `json:"currency"`
	TotalAmount      int        `json:"total_amount"`
	InvoicePayload   string     `json:"invoice_payload"`
	ShippingOptionID string     `json:"shipping_option_id"`
	OrderInfo        *OrderInfo `json:"order_info"`
}

// PassportData - Contains information about Telegram Passport data shared with the bot by the user.
type PassportData struct {
	Data        []*EncryptedPassportElement `json:"data"`
	Credentials *EncryptedCredentials       `json:"credentials"`
}

// PassportFile - This object represents a file uploaded to Telegram Passport. Currently all Telegram Passport files are in JPEG format when decrypted and don't exceed 10MB.
type PassportFile struct {
	FileID       string `json:"file_id"`
	FileUniqueID string `json:"file_unique_id"`
	FileSize     int    `json:"file_size"`
	FileDate     int    `json:"file_date"`
}

// EncryptedPassportElement - Contains information about documents or other Telegram Passport elements shared with the bot by the user.
type EncryptedPassportElement struct {
	Type        string          `json:"type"`
	Data        string          `json:"data"`
	PhoneNumber string          `json:"phone_number"`
	Email       string          `json:"email"`
	Files       []*PassportFile `json:"files"`
	FrontSide   *PassportFile   `json:"front_side"`
	ReverseSide *PassportFile   `json:"reverse_side"`
	Selfie      *PassportFile   `json:"selfie"`
	Translation []*PassportFile `json:"translation"`
	Hash        string          `json:"hash"`
}

// EncryptedCredentials - Contains data required for decrypting and authenticating EncryptedPassportElement. See the Telegram Passport Documentation for a complete description of the data decryption and authentication processes.
type EncryptedCredentials struct {
	Data   string `json:"data"`
	Hash   string `json:"hash"`
	Secret string `json:"secret"`
}

// PassportElementErrorDataField - Represents an issue in one of the data fields that was provided by the user. The error is considered resolved when the field's value changes.
type PassportElementErrorDataField struct {
	Source    string `json:"source"`
	Type      string `json:"type"`
	FieldName string `json:"field_name"`
	DataHash  string `json:"data_hash"`
	Message   string `json:"message"`
}

// PassportElementErrorFrontSide - Represents an issue with the front side of a document. The error is considered resolved when the file with the front side of the document changes.
type PassportElementErrorFrontSide struct {
	Source   string `json:"source"`
	Type     string `json:"type"`
	FileHash string `json:"file_hash"`
	Message  string `json:"message"`
}

// PassportElementErrorReverseSide - Represents an issue with the reverse side of a document. The error is considered resolved when the file with reverse side of the document changes.
type PassportElementErrorReverseSide struct {
	Source   string `json:"source"`
	Type     string `json:"type"`
	FileHash string `json:"file_hash"`
	Message  string `json:"message"`
}

// PassportElementErrorSelfie - Represents an issue with the selfie with a document. The error is considered resolved when the file with the selfie changes.
type PassportElementErrorSelfie struct {
	Source   string `json:"source"`
	Type     string `json:"type"`
	FileHash string `json:"file_hash"`
	Message  string `json:"message"`
}

// PassportElementErrorFile - Represents an issue with a document scan. The error is considered resolved when the file with the document scan changes.
type PassportElementErrorFile struct {
	Source   string `json:"source"`
	Type     string `json:"type"`
	FileHash string `json:"file_hash"`
	Message  string `json:"message"`
}

// PassportElementErrorFiles - Represents an issue with a list of scans. The error is considered resolved when the list of files containing the scans changes.
type PassportElementErrorFiles struct {
	Source     string   `json:"source"`
	Type       string   `json:"type"`
	FileHashes []string `json:"file_hashes"`
	Message    string   `json:"message"`
}

// PassportElementErrorTranslationFile - Represents an issue with one of the files that constitute the translation of a document. The error is considered resolved when the file changes.
type PassportElementErrorTranslationFile struct {
	Source   string `json:"source"`
	Type     string `json:"type"`
	FileHash string `json:"file_hash"`
	Message  string `json:"message"`
}

// PassportElementErrorTranslationFiles - Represents an issue with the translated version of a document. The error is considered resolved when a file with the document translation change.
type PassportElementErrorTranslationFiles struct {
	Source     string   `json:"source"`
	Type       string   `json:"type"`
	FileHashes []string `json:"file_hashes"`
	Message    string   `json:"message"`
}

// PassportElementErrorUnspecified - Represents an issue in an unspecified place. The error is considered resolved when new data is added.
type PassportElementErrorUnspecified struct {
	Source      string `json:"source"`
	Type        string `json:"type"`
	ElementHash string `json:"element_hash"`
	Message     string `json:"message"`
}

// Game - This object represents a game. Use BotFather to create and edit games, their short names will act as unique identifiers.
type Game struct {
	Title        string           `json:"title"`
	Description  string           `json:"description"`
	Photo        []*PhotoSize     `json:"photo"`
	Text         string           `json:"text"`
	TextEntities []*MessageEntity `json:"text_entities"`
	Animation    *Animation       `json:"animation"`
}

// CallbackGame - A placeholder, currently holds no information. Use BotFather to set up your game.
type CallbackGame struct {
}

// GameHighScore - This object represents one row of the high scores table for a game.
type GameHighScore struct {
	Position int   `json:"position"`
	User     *User `json:"user"`
	Score    int   `json:"score"`
}

// GetUpdatesMethod represents Telegram Bot API method getUpdates
// Use this method to receive incoming updates using long polling (wiki). An Array of Update objects is returned.
type GetUpdatesMethod struct {
	Offset         int      `json:"offset"`
	Limit          int      `json:"limit"`
	Timeout        int      `json:"timeout"`
	AllowedUpdates []string `json:"allowed_updates"`
}

// GetUpdates creates new Telegram Bot API method getUpdates
// Use this method to receive incoming updates using long polling (wiki). An Array of Update objects is returned.
func GetUpdates() *GetUpdatesMethod {
	return &GetUpdatesMethod{}
}

// SetOffset - Identifier of the first update to be returned. Must be greater by one than the highest among the identifiers of previously received updates. By default, updates starting with the earliest unconfirmed update are returned. An update is considered confirmed as soon as getUpdates is called with an offset higher than its update_id. The negative offset can be specified to retrieve updates starting from -offset update from the end of the updates queue. All previous updates will forgotten.
func (m *GetUpdatesMethod) SetOffset(value int) *GetUpdatesMethod {
	m.Offset = value
	return m
}

// SetLimit - Limits the number of updates to be retrieved. Values between 1-100 are accepted. Defaults to 100.
func (m *GetUpdatesMethod) SetLimit(value int) *GetUpdatesMethod {
	m.Limit = value
	return m
}

// SetTimeout - Timeout in seconds for long polling. Defaults to 0, i.e. usual short polling. Should be positive, short polling should be used for testing purposes only.
func (m *GetUpdatesMethod) SetTimeout(value int) *GetUpdatesMethod {
	m.Timeout = value
	return m
}

// SetAllowedUpdates - A JSON-serialized list of the update types you want your bot to receive. For example, specify [“message”, “edited_channel_post”, “callback_query”] to only receive updates of these types. See Update for a complete list of available update types. Specify an empty list to receive all updates regardless of type (default). If not specified, the previous setting will be used.Please note that this parameter doesn't affect updates created before the call to the getUpdates, so unwanted updates may be received for a short period of time.
func (m *GetUpdatesMethod) SetAllowedUpdates(value []string) *GetUpdatesMethod {
	m.AllowedUpdates = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetUpdatesMethod) Send(client *Client) (res []*Update, err error) {
	return res, client.request("getUpdates", m, &res)
}

// SetWebhookMethod represents Telegram Bot API method setWebhook
// Use this method to specify a url and receive incoming updates via an outgoing webhook. Whenever there is an update for the bot, we will send an HTTPS POST request to the specified url, containing a JSON-serialized Update. In case of an unsuccessful request, we will give up after a reasonable amount of attempts. Returns True on success.
type SetWebhookMethod struct {
	URL                string    `json:"url" required:"true"`
	Certificate        InputFile `json:"certificate"`
	IPAddress          string    `json:"ip_address"`
	MaxConnections     int       `json:"max_connections"`
	AllowedUpdates     []string  `json:"allowed_updates"`
	DropPendingUpdates bool      `json:"drop_pending_updates"`
}

// SetWebhook creates new Telegram Bot API method setWebhook
// Use this method to specify a url and receive incoming updates via an outgoing webhook. Whenever there is an update for the bot, we will send an HTTPS POST request to the specified url, containing a JSON-serialized Update. In case of an unsuccessful request, we will give up after a reasonable amount of attempts. Returns True on success.
func SetWebhook(uRL string) *SetWebhookMethod {
	return &SetWebhookMethod{
		URL: uRL,
	}
}

// SetURL - HTTPS url to send updates to. Use an empty string to remove webhook integration
func (m *SetWebhookMethod) SetURL(value string) *SetWebhookMethod {
	m.URL = value
	return m
}

// SetCertificate - Upload your public key certificate so that the root certificate in use can be checked. See our self-signed guide for details.
func (m *SetWebhookMethod) SetCertificate(value InputFile) *SetWebhookMethod {
	m.Certificate = value
	return m
}

// SetIPAddress - The fixed IP address which will be used to send webhook requests instead of the IP address resolved through DNS
func (m *SetWebhookMethod) SetIPAddress(value string) *SetWebhookMethod {
	m.IPAddress = value
	return m
}

// SetMaxConnections - Maximum allowed number of simultaneous HTTPS connections to the webhook for update delivery, 1-100. Defaults to 40. Use lower values to limit the load on your bot's server, and higher values to increase your bot's throughput.
func (m *SetWebhookMethod) SetMaxConnections(value int) *SetWebhookMethod {
	m.MaxConnections = value
	return m
}

// SetAllowedUpdates - A JSON-serialized list of the update types you want your bot to receive. For example, specify [“message”, “edited_channel_post”, “callback_query”] to only receive updates of these types. See Update for a complete list of available update types. Specify an empty list to receive all updates regardless of type (default). If not specified, the previous setting will be used.Please note that this parameter doesn't affect updates created before the call to the setWebhook, so unwanted updates may be received for a short period of time.
func (m *SetWebhookMethod) SetAllowedUpdates(value []string) *SetWebhookMethod {
	m.AllowedUpdates = value
	return m
}

// SetDropPendingUpdates - Pass True to drop all pending updates
func (m *SetWebhookMethod) SetDropPendingUpdates(value bool) *SetWebhookMethod {
	m.DropPendingUpdates = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetWebhookMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setWebhook", m, &res)
}

// DeleteWebhookMethod represents Telegram Bot API method deleteWebhook
// Use this method to remove webhook integration if you decide to switch back to getUpdates. Returns True on success.
type DeleteWebhookMethod struct {
	DropPendingUpdates bool `json:"drop_pending_updates"`
}

// DeleteWebhook creates new Telegram Bot API method deleteWebhook
// Use this method to remove webhook integration if you decide to switch back to getUpdates. Returns True on success.
func DeleteWebhook() *DeleteWebhookMethod {
	return &DeleteWebhookMethod{}
}

// SetDropPendingUpdates - Pass True to drop all pending updates
func (m *DeleteWebhookMethod) SetDropPendingUpdates(value bool) *DeleteWebhookMethod {
	m.DropPendingUpdates = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *DeleteWebhookMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("deleteWebhook", m, &res)
}

// GetWebhookInfoMethod represents Telegram Bot API method getWebhookInfo
// Use this method to get current webhook status. Requires no parameters. On success, returns a WebhookInfo object. If the bot is using getUpdates, will return an object with the url field empty.
type GetWebhookInfoMethod struct {
}

// GetWebhookInfo creates new Telegram Bot API method getWebhookInfo
// Use this method to get current webhook status. Requires no parameters. On success, returns a WebhookInfo object. If the bot is using getUpdates, will return an object with the url field empty.
func GetWebhookInfo() *GetWebhookInfoMethod {
	return &GetWebhookInfoMethod{}
}

// Send is used to send request using Telegram Bot Client
func (m *GetWebhookInfoMethod) Send(client *Client) (res *WebhookInfo, err error) {
	return res, client.request("getWebhookInfo", m, &res)
}

// GetMeMethod represents Telegram Bot API method getMe
// A simple method for testing your bot's auth token. Requires no parameters. Returns basic information about the bot in form of a User object.
type GetMeMethod struct {
}

// GetMe creates new Telegram Bot API method getMe
// A simple method for testing your bot's auth token. Requires no parameters. Returns basic information about the bot in form of a User object.
func GetMe() *GetMeMethod {
	return &GetMeMethod{}
}

// Send is used to send request using Telegram Bot Client
func (m *GetMeMethod) Send(client *Client) (res *User, err error) {
	return res, client.request("getMe", m, &res)
}

// LogOutMethod represents Telegram Bot API method logOut
// Use this method to log out from the cloud Bot API server before launching the bot locally. You must log out the bot before running it locally, otherwise there is no guarantee that the bot will receive updates. After a successful call, you can immediately log in on a local server, but will not be able to log in back to the cloud Bot API server for 10 minutes. Returns True on success. Requires no parameters.
type LogOutMethod struct {
}

// LogOut creates new Telegram Bot API method logOut
// Use this method to log out from the cloud Bot API server before launching the bot locally. You must log out the bot before running it locally, otherwise there is no guarantee that the bot will receive updates. After a successful call, you can immediately log in on a local server, but will not be able to log in back to the cloud Bot API server for 10 minutes. Returns True on success. Requires no parameters.
func LogOut() *LogOutMethod {
	return &LogOutMethod{}
}

// Send is used to send request using Telegram Bot Client
func (m *LogOutMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("logOut", m, &res)
}

// CloseMethod represents Telegram Bot API method close
// Use this method to close the bot instance before moving it from one local server to another. You need to delete the webhook before calling this method to ensure that the bot isn't launched again after server restart. The method will return error 429 in the first 10 minutes after the bot is launched. Returns True on success. Requires no parameters.
type CloseMethod struct {
}

// Close creates new Telegram Bot API method close
// Use this method to close the bot instance before moving it from one local server to another. You need to delete the webhook before calling this method to ensure that the bot isn't launched again after server restart. The method will return error 429 in the first 10 minutes after the bot is launched. Returns True on success. Requires no parameters.
func Close() *CloseMethod {
	return &CloseMethod{}
}

// Send is used to send request using Telegram Bot Client
func (m *CloseMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("close", m, &res)
}

// SendMessageMethod represents Telegram Bot API method sendMessage
// Use this method to send text messages. On success, the sent Message is returned.
type SendMessageMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	Text                     string           `json:"text" required:"true"`
	ParseMode                string           `json:"parse_mode"`
	Entities                 []*MessageEntity `json:"entities"`
	DisableWebPagePreview    bool             `json:"disable_web_page_preview"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// SendMessage creates new Telegram Bot API method sendMessage
// Use this method to send text messages. On success, the sent Message is returned.
func SendMessage(chatID ChatID, text string) *SendMessageMethod {
	return &SendMessageMethod{
		ChatID: chatID,
		Text:   text,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendMessageMethod) SetChatID(value ChatID) *SendMessageMethod {
	m.ChatID = value
	return m
}

// SetText - Text of the message to be sent, 1-4096 characters after entities parsing
func (m *SendMessageMethod) SetText(value string) *SendMessageMethod {
	m.Text = value
	return m
}

// SetParseMode - Mode for parsing entities in the message text. See formatting options for more details.
func (m *SendMessageMethod) SetParseMode(value string) *SendMessageMethod {
	m.ParseMode = value
	return m
}

// SetEntities - List of special entities that appear in message text, which can be specified instead of parse_mode
func (m *SendMessageMethod) SetEntities(value []*MessageEntity) *SendMessageMethod {
	m.Entities = value
	return m
}

// SetDisableWebPagePreview - Disables link previews for links in this message
func (m *SendMessageMethod) SetDisableWebPagePreview(value bool) *SendMessageMethod {
	m.DisableWebPagePreview = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendMessageMethod) SetDisableNotification(value bool) *SendMessageMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendMessageMethod) SetReplyToMessageID(value int) *SendMessageMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendMessageMethod) SetAllowSendingWithoutReply(value bool) *SendMessageMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendMessageMethod) SetReplyMarkup(value ReplyMarkup) *SendMessageMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendMessageMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendMessage", m, &res)
}

// ForwardMessageMethod represents Telegram Bot API method forwardMessage
// Use this method to forward messages of any kind. On success, the sent Message is returned.
type ForwardMessageMethod struct {
	ChatID              ChatID `json:"chat_id" required:"true"`
	FromChatID          ChatID `json:"from_chat_id" required:"true"`
	DisableNotification bool   `json:"disable_notification"`
	MessageID           int    `json:"message_id" required:"true"`
}

// ForwardMessage creates new Telegram Bot API method forwardMessage
// Use this method to forward messages of any kind. On success, the sent Message is returned.
func ForwardMessage(chatID ChatID, fromChatID ChatID, messageID int) *ForwardMessageMethod {
	return &ForwardMessageMethod{
		ChatID:     chatID,
		FromChatID: fromChatID,
		MessageID:  messageID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *ForwardMessageMethod) SetChatID(value ChatID) *ForwardMessageMethod {
	m.ChatID = value
	return m
}

// SetFromChatID - Unique identifier for the chat where the original message was sent (or channel username in the format @channelusername)
func (m *ForwardMessageMethod) SetFromChatID(value ChatID) *ForwardMessageMethod {
	m.FromChatID = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *ForwardMessageMethod) SetDisableNotification(value bool) *ForwardMessageMethod {
	m.DisableNotification = value
	return m
}

// SetMessageID - Message identifier in the chat specified in from_chat_id
func (m *ForwardMessageMethod) SetMessageID(value int) *ForwardMessageMethod {
	m.MessageID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *ForwardMessageMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("forwardMessage", m, &res)
}

// CopyMessageMethod represents Telegram Bot API method copyMessage
// Use this method to copy messages of any kind. The method is analogous to the method forwardMessages, but the copied message doesn't have a link to the original message. Returns the MessageId of the sent message on success.
type CopyMessageMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	FromChatID               ChatID           `json:"from_chat_id" required:"true"`
	MessageID                int              `json:"message_id" required:"true"`
	Caption                  string           `json:"caption"`
	ParseMode                string           `json:"parse_mode"`
	CaptionEntities          []*MessageEntity `json:"caption_entities"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// CopyMessage creates new Telegram Bot API method copyMessage
// Use this method to copy messages of any kind. The method is analogous to the method forwardMessages, but the copied message doesn't have a link to the original message. Returns the MessageId of the sent message on success.
func CopyMessage(chatID ChatID, fromChatID ChatID, messageID int) *CopyMessageMethod {
	return &CopyMessageMethod{
		ChatID:     chatID,
		FromChatID: fromChatID,
		MessageID:  messageID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *CopyMessageMethod) SetChatID(value ChatID) *CopyMessageMethod {
	m.ChatID = value
	return m
}

// SetFromChatID - Unique identifier for the chat where the original message was sent (or channel username in the format @channelusername)
func (m *CopyMessageMethod) SetFromChatID(value ChatID) *CopyMessageMethod {
	m.FromChatID = value
	return m
}

// SetMessageID - Message identifier in the chat specified in from_chat_id
func (m *CopyMessageMethod) SetMessageID(value int) *CopyMessageMethod {
	m.MessageID = value
	return m
}

// SetCaption - New caption for media, 0-1024 characters after entities parsing. If not specified, the original caption is kept
func (m *CopyMessageMethod) SetCaption(value string) *CopyMessageMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the new caption. See formatting options for more details.
func (m *CopyMessageMethod) SetParseMode(value string) *CopyMessageMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the new caption, which can be specified instead of parse_mode
func (m *CopyMessageMethod) SetCaptionEntities(value []*MessageEntity) *CopyMessageMethod {
	m.CaptionEntities = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *CopyMessageMethod) SetDisableNotification(value bool) *CopyMessageMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *CopyMessageMethod) SetReplyToMessageID(value int) *CopyMessageMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *CopyMessageMethod) SetAllowSendingWithoutReply(value bool) *CopyMessageMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *CopyMessageMethod) SetReplyMarkup(value ReplyMarkup) *CopyMessageMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *CopyMessageMethod) Send(client *Client) (res *MessageID, err error) {
	return res, client.request("copyMessage", m, &res)
}

// SendPhotoMethod represents Telegram Bot API method sendPhoto
// Use this method to send photos. On success, the sent Message is returned.
type SendPhotoMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	Photo                    InputFile        `json:"photo" required:"true"`
	Caption                  string           `json:"caption"`
	ParseMode                string           `json:"parse_mode"`
	CaptionEntities          []*MessageEntity `json:"caption_entities"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// SendPhoto creates new Telegram Bot API method sendPhoto
// Use this method to send photos. On success, the sent Message is returned.
func SendPhoto(chatID ChatID, photo InputFile) *SendPhotoMethod {
	return &SendPhotoMethod{
		ChatID: chatID,
		Photo:  photo,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendPhotoMethod) SetChatID(value ChatID) *SendPhotoMethod {
	m.ChatID = value
	return m
}

// SetPhoto - Photo to send. Pass a file_id as String to send a photo that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a photo from the Internet, or upload a new photo using multipart/form-data. More info on Sending Files »
func (m *SendPhotoMethod) SetPhoto(value InputFile) *SendPhotoMethod {
	m.Photo = value
	return m
}

// SetCaption - Photo caption (may also be used when resending photos by file_id), 0-1024 characters after entities parsing
func (m *SendPhotoMethod) SetCaption(value string) *SendPhotoMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the photo caption. See formatting options for more details.
func (m *SendPhotoMethod) SetParseMode(value string) *SendPhotoMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
func (m *SendPhotoMethod) SetCaptionEntities(value []*MessageEntity) *SendPhotoMethod {
	m.CaptionEntities = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendPhotoMethod) SetDisableNotification(value bool) *SendPhotoMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendPhotoMethod) SetReplyToMessageID(value int) *SendPhotoMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendPhotoMethod) SetAllowSendingWithoutReply(value bool) *SendPhotoMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendPhotoMethod) SetReplyMarkup(value ReplyMarkup) *SendPhotoMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendPhotoMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendPhoto", m, &res)
}

// SendAudioMethod represents Telegram Bot API method sendAudio
// Use this method to send audio files, if you want Telegram clients to display them in the music player. Your audio must be in the .MP3 or .M4A format. On success, the sent Message is returned. Bots can currently send audio files of up to 50 MB in size, this limit may be changed in the future.
type SendAudioMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	Audio                    InputFile        `json:"audio" required:"true"`
	Caption                  string           `json:"caption"`
	ParseMode                string           `json:"parse_mode"`
	CaptionEntities          []*MessageEntity `json:"caption_entities"`
	Duration                 int              `json:"duration"`
	Performer                string           `json:"performer"`
	Title                    string           `json:"title"`
	Thumb                    InputFile        `json:"thumb"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// SendAudio creates new Telegram Bot API method sendAudio
// Use this method to send audio files, if you want Telegram clients to display them in the music player. Your audio must be in the .MP3 or .M4A format. On success, the sent Message is returned. Bots can currently send audio files of up to 50 MB in size, this limit may be changed in the future.
func SendAudio(chatID ChatID, audio InputFile) *SendAudioMethod {
	return &SendAudioMethod{
		ChatID: chatID,
		Audio:  audio,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendAudioMethod) SetChatID(value ChatID) *SendAudioMethod {
	m.ChatID = value
	return m
}

// SetAudio - Audio file to send. Pass a file_id as String to send an audio file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an audio file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
func (m *SendAudioMethod) SetAudio(value InputFile) *SendAudioMethod {
	m.Audio = value
	return m
}

// SetCaption - Audio caption, 0-1024 characters after entities parsing
func (m *SendAudioMethod) SetCaption(value string) *SendAudioMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the audio caption. See formatting options for more details.
func (m *SendAudioMethod) SetParseMode(value string) *SendAudioMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
func (m *SendAudioMethod) SetCaptionEntities(value []*MessageEntity) *SendAudioMethod {
	m.CaptionEntities = value
	return m
}

// SetDuration - Duration of the audio in seconds
func (m *SendAudioMethod) SetDuration(value int) *SendAudioMethod {
	m.Duration = value
	return m
}

// SetPerformer - Performer
func (m *SendAudioMethod) SetPerformer(value string) *SendAudioMethod {
	m.Performer = value
	return m
}

// SetTitle - Track name
func (m *SendAudioMethod) SetTitle(value string) *SendAudioMethod {
	m.Title = value
	return m
}

// SetThumb - Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
func (m *SendAudioMethod) SetThumb(value InputFile) *SendAudioMethod {
	m.Thumb = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendAudioMethod) SetDisableNotification(value bool) *SendAudioMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendAudioMethod) SetReplyToMessageID(value int) *SendAudioMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendAudioMethod) SetAllowSendingWithoutReply(value bool) *SendAudioMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendAudioMethod) SetReplyMarkup(value ReplyMarkup) *SendAudioMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendAudioMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendAudio", m, &res)
}

// SendDocumentMethod represents Telegram Bot API method sendDocument
// Use this method to send general files. On success, the sent Message is returned. Bots can currently send files of any type of up to 50 MB in size, this limit may be changed in the future.
type SendDocumentMethod struct {
	ChatID                      ChatID           `json:"chat_id" required:"true"`
	Document                    InputFile        `json:"document" required:"true"`
	Thumb                       InputFile        `json:"thumb"`
	Caption                     string           `json:"caption"`
	ParseMode                   string           `json:"parse_mode"`
	CaptionEntities             []*MessageEntity `json:"caption_entities"`
	DisableContentTypeDetection bool             `json:"disable_content_type_detection"`
	DisableNotification         bool             `json:"disable_notification"`
	ReplyToMessageID            int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply    bool             `json:"allow_sending_without_reply"`
	ReplyMarkup                 ReplyMarkup      `json:"reply_markup"`
}

// SendDocument creates new Telegram Bot API method sendDocument
// Use this method to send general files. On success, the sent Message is returned. Bots can currently send files of any type of up to 50 MB in size, this limit may be changed in the future.
func SendDocument(chatID ChatID, document InputFile) *SendDocumentMethod {
	return &SendDocumentMethod{
		ChatID:   chatID,
		Document: document,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendDocumentMethod) SetChatID(value ChatID) *SendDocumentMethod {
	m.ChatID = value
	return m
}

// SetDocument - File to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
func (m *SendDocumentMethod) SetDocument(value InputFile) *SendDocumentMethod {
	m.Document = value
	return m
}

// SetThumb - Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
func (m *SendDocumentMethod) SetThumb(value InputFile) *SendDocumentMethod {
	m.Thumb = value
	return m
}

// SetCaption - Document caption (may also be used when resending documents by file_id), 0-1024 characters after entities parsing
func (m *SendDocumentMethod) SetCaption(value string) *SendDocumentMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the document caption. See formatting options for more details.
func (m *SendDocumentMethod) SetParseMode(value string) *SendDocumentMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
func (m *SendDocumentMethod) SetCaptionEntities(value []*MessageEntity) *SendDocumentMethod {
	m.CaptionEntities = value
	return m
}

// SetDisableContentTypeDetection - Disables automatic server-side content type detection for files uploaded using multipart/form-data
func (m *SendDocumentMethod) SetDisableContentTypeDetection(value bool) *SendDocumentMethod {
	m.DisableContentTypeDetection = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendDocumentMethod) SetDisableNotification(value bool) *SendDocumentMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendDocumentMethod) SetReplyToMessageID(value int) *SendDocumentMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendDocumentMethod) SetAllowSendingWithoutReply(value bool) *SendDocumentMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendDocumentMethod) SetReplyMarkup(value ReplyMarkup) *SendDocumentMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendDocumentMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendDocument", m, &res)
}

// SendVideoMethod represents Telegram Bot API method sendVideo
// Use this method to send video files, Telegram clients support mp4 videos (other formats may be sent as Document). On success, the sent Message is returned. Bots can currently send video files of up to 50 MB in size, this limit may be changed in the future.
type SendVideoMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	Video                    InputFile        `json:"video" required:"true"`
	Duration                 int              `json:"duration"`
	Width                    int              `json:"width"`
	Height                   int              `json:"height"`
	Thumb                    InputFile        `json:"thumb"`
	Caption                  string           `json:"caption"`
	ParseMode                string           `json:"parse_mode"`
	CaptionEntities          []*MessageEntity `json:"caption_entities"`
	SupportsStreaming        bool             `json:"supports_streaming"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// SendVideo creates new Telegram Bot API method sendVideo
// Use this method to send video files, Telegram clients support mp4 videos (other formats may be sent as Document). On success, the sent Message is returned. Bots can currently send video files of up to 50 MB in size, this limit may be changed in the future.
func SendVideo(chatID ChatID, video InputFile) *SendVideoMethod {
	return &SendVideoMethod{
		ChatID: chatID,
		Video:  video,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendVideoMethod) SetChatID(value ChatID) *SendVideoMethod {
	m.ChatID = value
	return m
}

// SetVideo - Video to send. Pass a file_id as String to send a video that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a video from the Internet, or upload a new video using multipart/form-data. More info on Sending Files »
func (m *SendVideoMethod) SetVideo(value InputFile) *SendVideoMethod {
	m.Video = value
	return m
}

// SetDuration - Duration of sent video in seconds
func (m *SendVideoMethod) SetDuration(value int) *SendVideoMethod {
	m.Duration = value
	return m
}

// SetWidth - Video width
func (m *SendVideoMethod) SetWidth(value int) *SendVideoMethod {
	m.Width = value
	return m
}

// SetHeight - Video height
func (m *SendVideoMethod) SetHeight(value int) *SendVideoMethod {
	m.Height = value
	return m
}

// SetThumb - Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
func (m *SendVideoMethod) SetThumb(value InputFile) *SendVideoMethod {
	m.Thumb = value
	return m
}

// SetCaption - Video caption (may also be used when resending videos by file_id), 0-1024 characters after entities parsing
func (m *SendVideoMethod) SetCaption(value string) *SendVideoMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the video caption. See formatting options for more details.
func (m *SendVideoMethod) SetParseMode(value string) *SendVideoMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
func (m *SendVideoMethod) SetCaptionEntities(value []*MessageEntity) *SendVideoMethod {
	m.CaptionEntities = value
	return m
}

// SetSupportsStreaming - Pass True, if the uploaded video is suitable for streaming
func (m *SendVideoMethod) SetSupportsStreaming(value bool) *SendVideoMethod {
	m.SupportsStreaming = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendVideoMethod) SetDisableNotification(value bool) *SendVideoMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendVideoMethod) SetReplyToMessageID(value int) *SendVideoMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendVideoMethod) SetAllowSendingWithoutReply(value bool) *SendVideoMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendVideoMethod) SetReplyMarkup(value ReplyMarkup) *SendVideoMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendVideoMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendVideo", m, &res)
}

// SendAnimationMethod represents Telegram Bot API method sendAnimation
// Use this method to send animation files (GIF or H.264/MPEG-4 AVC video without sound). On success, the sent Message is returned. Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future.
type SendAnimationMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	Animation                InputFile        `json:"animation" required:"true"`
	Duration                 int              `json:"duration"`
	Width                    int              `json:"width"`
	Height                   int              `json:"height"`
	Thumb                    InputFile        `json:"thumb"`
	Caption                  string           `json:"caption"`
	ParseMode                string           `json:"parse_mode"`
	CaptionEntities          []*MessageEntity `json:"caption_entities"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// SendAnimation creates new Telegram Bot API method sendAnimation
// Use this method to send animation files (GIF or H.264/MPEG-4 AVC video without sound). On success, the sent Message is returned. Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future.
func SendAnimation(chatID ChatID, animation InputFile) *SendAnimationMethod {
	return &SendAnimationMethod{
		ChatID:    chatID,
		Animation: animation,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendAnimationMethod) SetChatID(value ChatID) *SendAnimationMethod {
	m.ChatID = value
	return m
}

// SetAnimation - Animation to send. Pass a file_id as String to send an animation that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an animation from the Internet, or upload a new animation using multipart/form-data. More info on Sending Files »
func (m *SendAnimationMethod) SetAnimation(value InputFile) *SendAnimationMethod {
	m.Animation = value
	return m
}

// SetDuration - Duration of sent animation in seconds
func (m *SendAnimationMethod) SetDuration(value int) *SendAnimationMethod {
	m.Duration = value
	return m
}

// SetWidth - Animation width
func (m *SendAnimationMethod) SetWidth(value int) *SendAnimationMethod {
	m.Width = value
	return m
}

// SetHeight - Animation height
func (m *SendAnimationMethod) SetHeight(value int) *SendAnimationMethod {
	m.Height = value
	return m
}

// SetThumb - Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
func (m *SendAnimationMethod) SetThumb(value InputFile) *SendAnimationMethod {
	m.Thumb = value
	return m
}

// SetCaption - Animation caption (may also be used when resending animation by file_id), 0-1024 characters after entities parsing
func (m *SendAnimationMethod) SetCaption(value string) *SendAnimationMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the animation caption. See formatting options for more details.
func (m *SendAnimationMethod) SetParseMode(value string) *SendAnimationMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
func (m *SendAnimationMethod) SetCaptionEntities(value []*MessageEntity) *SendAnimationMethod {
	m.CaptionEntities = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendAnimationMethod) SetDisableNotification(value bool) *SendAnimationMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendAnimationMethod) SetReplyToMessageID(value int) *SendAnimationMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendAnimationMethod) SetAllowSendingWithoutReply(value bool) *SendAnimationMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendAnimationMethod) SetReplyMarkup(value ReplyMarkup) *SendAnimationMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendAnimationMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendAnimation", m, &res)
}

// SendVoiceMethod represents Telegram Bot API method sendVoice
// Use this method to send audio files, if you want Telegram clients to display the file as a playable voice message. For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as Audio or Document). On success, the sent Message is returned. Bots can currently send voice messages of up to 50 MB in size, this limit may be changed in the future.
type SendVoiceMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	Voice                    InputFile        `json:"voice" required:"true"`
	Caption                  string           `json:"caption"`
	ParseMode                string           `json:"parse_mode"`
	CaptionEntities          []*MessageEntity `json:"caption_entities"`
	Duration                 int              `json:"duration"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// SendVoice creates new Telegram Bot API method sendVoice
// Use this method to send audio files, if you want Telegram clients to display the file as a playable voice message. For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as Audio or Document). On success, the sent Message is returned. Bots can currently send voice messages of up to 50 MB in size, this limit may be changed in the future.
func SendVoice(chatID ChatID, voice InputFile) *SendVoiceMethod {
	return &SendVoiceMethod{
		ChatID: chatID,
		Voice:  voice,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendVoiceMethod) SetChatID(value ChatID) *SendVoiceMethod {
	m.ChatID = value
	return m
}

// SetVoice - Audio file to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
func (m *SendVoiceMethod) SetVoice(value InputFile) *SendVoiceMethod {
	m.Voice = value
	return m
}

// SetCaption - Voice message caption, 0-1024 characters after entities parsing
func (m *SendVoiceMethod) SetCaption(value string) *SendVoiceMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the voice message caption. See formatting options for more details.
func (m *SendVoiceMethod) SetParseMode(value string) *SendVoiceMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
func (m *SendVoiceMethod) SetCaptionEntities(value []*MessageEntity) *SendVoiceMethod {
	m.CaptionEntities = value
	return m
}

// SetDuration - Duration of the voice message in seconds
func (m *SendVoiceMethod) SetDuration(value int) *SendVoiceMethod {
	m.Duration = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendVoiceMethod) SetDisableNotification(value bool) *SendVoiceMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendVoiceMethod) SetReplyToMessageID(value int) *SendVoiceMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendVoiceMethod) SetAllowSendingWithoutReply(value bool) *SendVoiceMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendVoiceMethod) SetReplyMarkup(value ReplyMarkup) *SendVoiceMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendVoiceMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendVoice", m, &res)
}

// SendVideoNoteMethod represents Telegram Bot API method sendVideoNote
// As of v.4.0, Telegram clients support rounded square mp4 videos of up to 1 minute long. Use this method to send video messages. On success, the sent Message is returned.
type SendVideoNoteMethod struct {
	ChatID                   ChatID      `json:"chat_id" required:"true"`
	VideoNote                InputFile   `json:"video_note" required:"true"`
	Duration                 int         `json:"duration"`
	Length                   int         `json:"length"`
	Thumb                    InputFile   `json:"thumb"`
	DisableNotification      bool        `json:"disable_notification"`
	ReplyToMessageID         int         `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool        `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup `json:"reply_markup"`
}

// SendVideoNote creates new Telegram Bot API method sendVideoNote
// As of v.4.0, Telegram clients support rounded square mp4 videos of up to 1 minute long. Use this method to send video messages. On success, the sent Message is returned.
func SendVideoNote(chatID ChatID, videoNote InputFile) *SendVideoNoteMethod {
	return &SendVideoNoteMethod{
		ChatID:    chatID,
		VideoNote: videoNote,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendVideoNoteMethod) SetChatID(value ChatID) *SendVideoNoteMethod {
	m.ChatID = value
	return m
}

// SetVideoNote - Video note to send. Pass a file_id as String to send a video note that exists on the Telegram servers (recommended) or upload a new video using multipart/form-data. More info on Sending Files ». Sending video notes by a URL is currently unsupported
func (m *SendVideoNoteMethod) SetVideoNote(value InputFile) *SendVideoNoteMethod {
	m.VideoNote = value
	return m
}

// SetDuration - Duration of sent video in seconds
func (m *SendVideoNoteMethod) SetDuration(value int) *SendVideoNoteMethod {
	m.Duration = value
	return m
}

// SetLength - Video width and height, i.e. diameter of the video message
func (m *SendVideoNoteMethod) SetLength(value int) *SendVideoNoteMethod {
	m.Length = value
	return m
}

// SetThumb - Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More info on Sending Files »
func (m *SendVideoNoteMethod) SetThumb(value InputFile) *SendVideoNoteMethod {
	m.Thumb = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendVideoNoteMethod) SetDisableNotification(value bool) *SendVideoNoteMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendVideoNoteMethod) SetReplyToMessageID(value int) *SendVideoNoteMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendVideoNoteMethod) SetAllowSendingWithoutReply(value bool) *SendVideoNoteMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendVideoNoteMethod) SetReplyMarkup(value ReplyMarkup) *SendVideoNoteMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendVideoNoteMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendVideoNote", m, &res)
}

// SendMediaGroupMethod represents Telegram Bot API method sendMediaGroup
// Use this method to send a group of photos, videos, documents or audios as an album. Documents and audio files can be only grouped in an album with messages of the same type. On success, an array of Messages that were sent is returned.
type SendMediaGroupMethod struct {
	ChatID                   ChatID        `json:"chat_id" required:"true"`
	Media                    []*InputMedia `json:"media" required:"true"`
	DisableNotification      bool          `json:"disable_notification"`
	ReplyToMessageID         int           `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool          `json:"allow_sending_without_reply"`
}

// SendMediaGroup creates new Telegram Bot API method sendMediaGroup
// Use this method to send a group of photos, videos, documents or audios as an album. Documents and audio files can be only grouped in an album with messages of the same type. On success, an array of Messages that were sent is returned.
func SendMediaGroup(chatID ChatID, media []*InputMedia) *SendMediaGroupMethod {
	return &SendMediaGroupMethod{
		ChatID: chatID,
		Media:  media,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendMediaGroupMethod) SetChatID(value ChatID) *SendMediaGroupMethod {
	m.ChatID = value
	return m
}

// SetMedia - A JSON-serialized array describing messages to be sent, must include 2-10 items
func (m *SendMediaGroupMethod) SetMedia(value []*InputMedia) *SendMediaGroupMethod {
	m.Media = value
	return m
}

// SetDisableNotification - Sends messages silently. Users will receive a notification with no sound.
func (m *SendMediaGroupMethod) SetDisableNotification(value bool) *SendMediaGroupMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the messages are a reply, ID of the original message
func (m *SendMediaGroupMethod) SetReplyToMessageID(value int) *SendMediaGroupMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendMediaGroupMethod) SetAllowSendingWithoutReply(value bool) *SendMediaGroupMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendMediaGroupMethod) Send(client *Client) (res []*Message, err error) {
	return res, client.request("sendMediaGroup", m, &res)
}

// SendLocationMethod represents Telegram Bot API method sendLocation
// Use this method to send point on the map. On success, the sent Message is returned.
type SendLocationMethod struct {
	ChatID                   ChatID      `json:"chat_id" required:"true"`
	Latitude                 float64     `json:"latitude" required:"true"`
	Longitude                float64     `json:"longitude" required:"true"`
	HorizontalAccuracy       float64     `json:"horizontal_accuracy"`
	LivePeriod               int         `json:"live_period"`
	Heading                  int         `json:"heading"`
	ProximityAlertRadius     int         `json:"proximity_alert_radius"`
	DisableNotification      bool        `json:"disable_notification"`
	ReplyToMessageID         int         `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool        `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup `json:"reply_markup"`
}

// SendLocation creates new Telegram Bot API method sendLocation
// Use this method to send point on the map. On success, the sent Message is returned.
func SendLocation(chatID ChatID, latitude float64, longitude float64) *SendLocationMethod {
	return &SendLocationMethod{
		ChatID:    chatID,
		Latitude:  latitude,
		Longitude: longitude,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendLocationMethod) SetChatID(value ChatID) *SendLocationMethod {
	m.ChatID = value
	return m
}

// SetLatitude - Latitude of the location
func (m *SendLocationMethod) SetLatitude(value float64) *SendLocationMethod {
	m.Latitude = value
	return m
}

// SetLongitude - Longitude of the location
func (m *SendLocationMethod) SetLongitude(value float64) *SendLocationMethod {
	m.Longitude = value
	return m
}

// SetHorizontalAccuracy - The radius of uncertainty for the location, measured in meters; 0-1500
func (m *SendLocationMethod) SetHorizontalAccuracy(value float64) *SendLocationMethod {
	m.HorizontalAccuracy = value
	return m
}

// SetLivePeriod - Period in seconds for which the location will be updated (see Live Locations, should be between 60 and 86400.
func (m *SendLocationMethod) SetLivePeriod(value int) *SendLocationMethod {
	m.LivePeriod = value
	return m
}

// SetHeading - For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
func (m *SendLocationMethod) SetHeading(value int) *SendLocationMethod {
	m.Heading = value
	return m
}

// SetProximityAlertRadius - For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
func (m *SendLocationMethod) SetProximityAlertRadius(value int) *SendLocationMethod {
	m.ProximityAlertRadius = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendLocationMethod) SetDisableNotification(value bool) *SendLocationMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendLocationMethod) SetReplyToMessageID(value int) *SendLocationMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendLocationMethod) SetAllowSendingWithoutReply(value bool) *SendLocationMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendLocationMethod) SetReplyMarkup(value ReplyMarkup) *SendLocationMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendLocationMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendLocation", m, &res)
}

// EditMessageLiveLocationMethod represents Telegram Bot API method editMessageLiveLocation
// Use this method to edit live location messages. A location can be edited until its live_period expires or editing is explicitly disabled by a call to stopMessageLiveLocation. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type EditMessageLiveLocationMethod struct {
	ChatID               ChatID                `json:"chat_id"`
	MessageID            int                   `json:"message_id"`
	InlineMessageID      string                `json:"inline_message_id"`
	Latitude             float64               `json:"latitude" required:"true"`
	Longitude            float64               `json:"longitude" required:"true"`
	HorizontalAccuracy   float64               `json:"horizontal_accuracy"`
	Heading              int                   `json:"heading"`
	ProximityAlertRadius int                   `json:"proximity_alert_radius"`
	ReplyMarkup          *InlineKeyboardMarkup `json:"reply_markup"`
}

// EditMessageLiveLocation creates new Telegram Bot API method editMessageLiveLocation
// Use this method to edit live location messages. A location can be edited until its live_period expires or editing is explicitly disabled by a call to stopMessageLiveLocation. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
func EditMessageLiveLocation(latitude float64, longitude float64) *EditMessageLiveLocationMethod {
	return &EditMessageLiveLocationMethod{
		Latitude:  latitude,
		Longitude: longitude,
	}
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *EditMessageLiveLocationMethod) SetChatID(value ChatID) *EditMessageLiveLocationMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the message to edit
func (m *EditMessageLiveLocationMethod) SetMessageID(value int) *EditMessageLiveLocationMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *EditMessageLiveLocationMethod) SetInlineMessageID(value string) *EditMessageLiveLocationMethod {
	m.InlineMessageID = value
	return m
}

// SetLatitude - Latitude of new location
func (m *EditMessageLiveLocationMethod) SetLatitude(value float64) *EditMessageLiveLocationMethod {
	m.Latitude = value
	return m
}

// SetLongitude - Longitude of new location
func (m *EditMessageLiveLocationMethod) SetLongitude(value float64) *EditMessageLiveLocationMethod {
	m.Longitude = value
	return m
}

// SetHorizontalAccuracy - The radius of uncertainty for the location, measured in meters; 0-1500
func (m *EditMessageLiveLocationMethod) SetHorizontalAccuracy(value float64) *EditMessageLiveLocationMethod {
	m.HorizontalAccuracy = value
	return m
}

// SetHeading - Direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
func (m *EditMessageLiveLocationMethod) SetHeading(value int) *EditMessageLiveLocationMethod {
	m.Heading = value
	return m
}

// SetProximityAlertRadius - Maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
func (m *EditMessageLiveLocationMethod) SetProximityAlertRadius(value int) *EditMessageLiveLocationMethod {
	m.ProximityAlertRadius = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for a new inline keyboard.
func (m *EditMessageLiveLocationMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *EditMessageLiveLocationMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *EditMessageLiveLocationMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("editMessageLiveLocation", m, &res)
}

// StopMessageLiveLocationMethod represents Telegram Bot API method stopMessageLiveLocation
// Use this method to stop updating a live location message before live_period expires. On success, if the message was sent by the bot, the sent Message is returned, otherwise True is returned.
type StopMessageLiveLocationMethod struct {
	ChatID          ChatID                `json:"chat_id"`
	MessageID       int                   `json:"message_id"`
	InlineMessageID string                `json:"inline_message_id"`
	ReplyMarkup     *InlineKeyboardMarkup `json:"reply_markup"`
}

// StopMessageLiveLocation creates new Telegram Bot API method stopMessageLiveLocation
// Use this method to stop updating a live location message before live_period expires. On success, if the message was sent by the bot, the sent Message is returned, otherwise True is returned.
func StopMessageLiveLocation() *StopMessageLiveLocationMethod {
	return &StopMessageLiveLocationMethod{}
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *StopMessageLiveLocationMethod) SetChatID(value ChatID) *StopMessageLiveLocationMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the message with live location to stop
func (m *StopMessageLiveLocationMethod) SetMessageID(value int) *StopMessageLiveLocationMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *StopMessageLiveLocationMethod) SetInlineMessageID(value string) *StopMessageLiveLocationMethod {
	m.InlineMessageID = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for a new inline keyboard.
func (m *StopMessageLiveLocationMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *StopMessageLiveLocationMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *StopMessageLiveLocationMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("stopMessageLiveLocation", m, &res)
}

// SendVenueMethod represents Telegram Bot API method sendVenue
// Use this method to send information about a venue. On success, the sent Message is returned.
type SendVenueMethod struct {
	ChatID                   ChatID      `json:"chat_id" required:"true"`
	Latitude                 float64     `json:"latitude" required:"true"`
	Longitude                float64     `json:"longitude" required:"true"`
	Title                    string      `json:"title" required:"true"`
	Address                  string      `json:"address" required:"true"`
	FoursquareID             string      `json:"foursquare_id"`
	FoursquareType           string      `json:"foursquare_type"`
	GooglePlaceID            string      `json:"google_place_id"`
	GooglePlaceType          string      `json:"google_place_type"`
	DisableNotification      bool        `json:"disable_notification"`
	ReplyToMessageID         int         `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool        `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup `json:"reply_markup"`
}

// SendVenue creates new Telegram Bot API method sendVenue
// Use this method to send information about a venue. On success, the sent Message is returned.
func SendVenue(chatID ChatID, latitude float64, longitude float64, title string, address string) *SendVenueMethod {
	return &SendVenueMethod{
		ChatID:    chatID,
		Latitude:  latitude,
		Longitude: longitude,
		Title:     title,
		Address:   address,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendVenueMethod) SetChatID(value ChatID) *SendVenueMethod {
	m.ChatID = value
	return m
}

// SetLatitude - Latitude of the venue
func (m *SendVenueMethod) SetLatitude(value float64) *SendVenueMethod {
	m.Latitude = value
	return m
}

// SetLongitude - Longitude of the venue
func (m *SendVenueMethod) SetLongitude(value float64) *SendVenueMethod {
	m.Longitude = value
	return m
}

// SetTitle - Name of the venue
func (m *SendVenueMethod) SetTitle(value string) *SendVenueMethod {
	m.Title = value
	return m
}

// SetAddress - Address of the venue
func (m *SendVenueMethod) SetAddress(value string) *SendVenueMethod {
	m.Address = value
	return m
}

// SetFoursquareID - Foursquare identifier of the venue
func (m *SendVenueMethod) SetFoursquareID(value string) *SendVenueMethod {
	m.FoursquareID = value
	return m
}

// SetFoursquareType - Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
func (m *SendVenueMethod) SetFoursquareType(value string) *SendVenueMethod {
	m.FoursquareType = value
	return m
}

// SetGooglePlaceID - Google Places identifier of the venue
func (m *SendVenueMethod) SetGooglePlaceID(value string) *SendVenueMethod {
	m.GooglePlaceID = value
	return m
}

// SetGooglePlaceType - Google Places type of the venue. (See supported types.)
func (m *SendVenueMethod) SetGooglePlaceType(value string) *SendVenueMethod {
	m.GooglePlaceType = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendVenueMethod) SetDisableNotification(value bool) *SendVenueMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendVenueMethod) SetReplyToMessageID(value int) *SendVenueMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendVenueMethod) SetAllowSendingWithoutReply(value bool) *SendVenueMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendVenueMethod) SetReplyMarkup(value ReplyMarkup) *SendVenueMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendVenueMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendVenue", m, &res)
}

// SendContactMethod represents Telegram Bot API method sendContact
// Use this method to send phone contacts. On success, the sent Message is returned.
type SendContactMethod struct {
	ChatID                   ChatID      `json:"chat_id" required:"true"`
	PhoneNumber              string      `json:"phone_number" required:"true"`
	FirstName                string      `json:"first_name" required:"true"`
	LastName                 string      `json:"last_name"`
	Vcard                    string      `json:"vcard"`
	DisableNotification      bool        `json:"disable_notification"`
	ReplyToMessageID         int         `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool        `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup `json:"reply_markup"`
}

// SendContact creates new Telegram Bot API method sendContact
// Use this method to send phone contacts. On success, the sent Message is returned.
func SendContact(chatID ChatID, phoneNumber string, firstName string) *SendContactMethod {
	return &SendContactMethod{
		ChatID:      chatID,
		PhoneNumber: phoneNumber,
		FirstName:   firstName,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendContactMethod) SetChatID(value ChatID) *SendContactMethod {
	m.ChatID = value
	return m
}

// SetPhoneNumber - Contact's phone number
func (m *SendContactMethod) SetPhoneNumber(value string) *SendContactMethod {
	m.PhoneNumber = value
	return m
}

// SetFirstName - Contact's first name
func (m *SendContactMethod) SetFirstName(value string) *SendContactMethod {
	m.FirstName = value
	return m
}

// SetLastName - Contact's last name
func (m *SendContactMethod) SetLastName(value string) *SendContactMethod {
	m.LastName = value
	return m
}

// SetVcard - Additional data about the contact in the form of a vCard, 0-2048 bytes
func (m *SendContactMethod) SetVcard(value string) *SendContactMethod {
	m.Vcard = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendContactMethod) SetDisableNotification(value bool) *SendContactMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendContactMethod) SetReplyToMessageID(value int) *SendContactMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendContactMethod) SetAllowSendingWithoutReply(value bool) *SendContactMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove keyboard or to force a reply from the user.
func (m *SendContactMethod) SetReplyMarkup(value ReplyMarkup) *SendContactMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendContactMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendContact", m, &res)
}

// SendPollMethod represents Telegram Bot API method sendPoll
// Use this method to send a native poll. On success, the sent Message is returned.
type SendPollMethod struct {
	ChatID                   ChatID           `json:"chat_id" required:"true"`
	Question                 string           `json:"question" required:"true"`
	Options                  []string         `json:"options" required:"true"`
	IsAnonymous              bool             `json:"is_anonymous"`
	Type                     string           `json:"type"`
	AllowsMultipleAnswers    bool             `json:"allows_multiple_answers"`
	CorrectOptionID          int              `json:"correct_option_id"`
	Explanation              string           `json:"explanation"`
	ExplanationParseMode     string           `json:"explanation_parse_mode"`
	ExplanationEntities      []*MessageEntity `json:"explanation_entities"`
	OpenPeriod               int              `json:"open_period"`
	CloseDate                int              `json:"close_date"`
	IsClosed                 bool             `json:"is_closed"`
	DisableNotification      bool             `json:"disable_notification"`
	ReplyToMessageID         int              `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup      `json:"reply_markup"`
}

// SendPoll creates new Telegram Bot API method sendPoll
// Use this method to send a native poll. On success, the sent Message is returned.
func SendPoll(chatID ChatID, question string, options []string) *SendPollMethod {
	return &SendPollMethod{
		ChatID:   chatID,
		Question: question,
		Options:  options,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendPollMethod) SetChatID(value ChatID) *SendPollMethod {
	m.ChatID = value
	return m
}

// SetQuestion - Poll question, 1-300 characters
func (m *SendPollMethod) SetQuestion(value string) *SendPollMethod {
	m.Question = value
	return m
}

// SetOptions - A JSON-serialized list of answer options, 2-10 strings 1-100 characters each
func (m *SendPollMethod) SetOptions(value []string) *SendPollMethod {
	m.Options = value
	return m
}

// SetIsAnonymous - True, if the poll needs to be anonymous, defaults to True
func (m *SendPollMethod) SetIsAnonymous(value bool) *SendPollMethod {
	m.IsAnonymous = value
	return m
}

// SetType - Poll type, “quiz” or “regular”, defaults to “regular”
func (m *SendPollMethod) SetType(value string) *SendPollMethod {
	m.Type = value
	return m
}

// SetAllowsMultipleAnswers - True, if the poll allows multiple answers, ignored for polls in quiz mode, defaults to False
func (m *SendPollMethod) SetAllowsMultipleAnswers(value bool) *SendPollMethod {
	m.AllowsMultipleAnswers = value
	return m
}

// SetCorrectOptionID - 0-based identifier of the correct answer option, required for polls in quiz mode
func (m *SendPollMethod) SetCorrectOptionID(value int) *SendPollMethod {
	m.CorrectOptionID = value
	return m
}

// SetExplanation - Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters with at most 2 line feeds after entities parsing
func (m *SendPollMethod) SetExplanation(value string) *SendPollMethod {
	m.Explanation = value
	return m
}

// SetExplanationParseMode - Mode for parsing entities in the explanation. See formatting options for more details.
func (m *SendPollMethod) SetExplanationParseMode(value string) *SendPollMethod {
	m.ExplanationParseMode = value
	return m
}

// SetExplanationEntities - List of special entities that appear in the poll explanation, which can be specified instead of parse_mode
func (m *SendPollMethod) SetExplanationEntities(value []*MessageEntity) *SendPollMethod {
	m.ExplanationEntities = value
	return m
}

// SetOpenPeriod - Amount of time in seconds the poll will be active after creation, 5-600. Can't be used together with close_date.
func (m *SendPollMethod) SetOpenPeriod(value int) *SendPollMethod {
	m.OpenPeriod = value
	return m
}

// SetCloseDate - Point in time (Unix timestamp) when the poll will be automatically closed. Must be at least 5 and no more than 600 seconds in the future. Can't be used together with open_period.
func (m *SendPollMethod) SetCloseDate(value int) *SendPollMethod {
	m.CloseDate = value
	return m
}

// SetIsClosed - Pass True, if the poll needs to be immediately closed. This can be useful for poll preview.
func (m *SendPollMethod) SetIsClosed(value bool) *SendPollMethod {
	m.IsClosed = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendPollMethod) SetDisableNotification(value bool) *SendPollMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendPollMethod) SetReplyToMessageID(value int) *SendPollMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendPollMethod) SetAllowSendingWithoutReply(value bool) *SendPollMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendPollMethod) SetReplyMarkup(value ReplyMarkup) *SendPollMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendPollMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendPoll", m, &res)
}

// SendDiceMethod represents Telegram Bot API method sendDice
// Use this method to send an animated emoji that will display a random value. On success, the sent Message is returned.
type SendDiceMethod struct {
	ChatID                   ChatID      `json:"chat_id" required:"true"`
	Emoji                    string      `json:"emoji"`
	DisableNotification      bool        `json:"disable_notification"`
	ReplyToMessageID         int         `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool        `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup `json:"reply_markup"`
}

// SendDice creates new Telegram Bot API method sendDice
// Use this method to send an animated emoji that will display a random value. On success, the sent Message is returned.
func SendDice(chatID ChatID) *SendDiceMethod {
	return &SendDiceMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendDiceMethod) SetChatID(value ChatID) *SendDiceMethod {
	m.ChatID = value
	return m
}

// SetEmoji - Emoji on which the dice throw animation is based. Currently, must be one of “”, “”, “”, “”, or “”. Dice can have values 1-6 for “” and “”, values 1-5 for “” and “”, and values 1-64 for “”. Defaults to “”
func (m *SendDiceMethod) SetEmoji(value string) *SendDiceMethod {
	m.Emoji = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendDiceMethod) SetDisableNotification(value bool) *SendDiceMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendDiceMethod) SetReplyToMessageID(value int) *SendDiceMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendDiceMethod) SetAllowSendingWithoutReply(value bool) *SendDiceMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendDiceMethod) SetReplyMarkup(value ReplyMarkup) *SendDiceMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendDiceMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendDice", m, &res)
}

// SendChatActionMethod represents Telegram Bot API method sendChatAction
// Use this method when you need to tell the user that something is happening on the bot's side. The status is set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status). Returns True on success.
type SendChatActionMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
	Action string `json:"action" required:"true"`
}

// SendChatAction creates new Telegram Bot API method sendChatAction
// Use this method when you need to tell the user that something is happening on the bot's side. The status is set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status). Returns True on success.
func SendChatAction(chatID ChatID, action string) *SendChatActionMethod {
	return &SendChatActionMethod{
		ChatID: chatID,
		Action: action,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendChatActionMethod) SetChatID(value ChatID) *SendChatActionMethod {
	m.ChatID = value
	return m
}

// SetAction - Type of action to broadcast. Choose one, depending on what the user is about to receive: typing for text messages, upload_photo for photos, record_video or upload_video for videos, record_voice or upload_voice for voice notes, upload_document for general files, find_location for location data, record_video_note or upload_video_note for video notes.
func (m *SendChatActionMethod) SetAction(value string) *SendChatActionMethod {
	m.Action = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendChatActionMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("sendChatAction", m, &res)
}

// GetUserProfilePhotosMethod represents Telegram Bot API method getUserProfilePhotos
// Use this method to get a list of profile pictures for a user. Returns a UserProfilePhotos object.
type GetUserProfilePhotosMethod struct {
	UserID int `json:"user_id" required:"true"`
	Offset int `json:"offset"`
	Limit  int `json:"limit"`
}

// GetUserProfilePhotos creates new Telegram Bot API method getUserProfilePhotos
// Use this method to get a list of profile pictures for a user. Returns a UserProfilePhotos object.
func GetUserProfilePhotos(userID int) *GetUserProfilePhotosMethod {
	return &GetUserProfilePhotosMethod{
		UserID: userID,
	}
}

// SetUserID - Unique identifier of the target user
func (m *GetUserProfilePhotosMethod) SetUserID(value int) *GetUserProfilePhotosMethod {
	m.UserID = value
	return m
}

// SetOffset - Sequential number of the first photo to be returned. By default, all photos are returned.
func (m *GetUserProfilePhotosMethod) SetOffset(value int) *GetUserProfilePhotosMethod {
	m.Offset = value
	return m
}

// SetLimit - Limits the number of photos to be retrieved. Values between 1-100 are accepted. Defaults to 100.
func (m *GetUserProfilePhotosMethod) SetLimit(value int) *GetUserProfilePhotosMethod {
	m.Limit = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetUserProfilePhotosMethod) Send(client *Client) (res *UserProfilePhotos, err error) {
	return res, client.request("getUserProfilePhotos", m, &res)
}

// GetFileMethod represents Telegram Bot API method getFile
// Use this method to get basic info about a file and prepare it for downloading. For the moment, bots can download files of up to 20MB in size. On success, a File object is returned. The file can then be downloaded via the link https://api.telegram.org/file/bot<token>/<file_path>, where <file_path> is taken from the response. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile again.
type GetFileMethod struct {
	FileID string `json:"file_id" required:"true"`
}

// GetFile creates new Telegram Bot API method getFile
// Use this method to get basic info about a file and prepare it for downloading. For the moment, bots can download files of up to 20MB in size. On success, a File object is returned. The file can then be downloaded via the link https://api.telegram.org/file/bot<token>/<file_path>, where <file_path> is taken from the response. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile again.
func GetFile(fileID string) *GetFileMethod {
	return &GetFileMethod{
		FileID: fileID,
	}
}

// SetFileID - File identifier to get info about
func (m *GetFileMethod) SetFileID(value string) *GetFileMethod {
	m.FileID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetFileMethod) Send(client *Client) (res *File, err error) {
	return res, client.request("getFile", m, &res)
}

// KickChatMemberMethod represents Telegram Bot API method kickChatMember
// Use this method to kick a user from a group, a supergroup or a channel. In the case of supergroups and channels, the user will not be able to return to the group on their own using invite links, etc., unless unbanned first. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
type KickChatMemberMethod struct {
	ChatID    ChatID `json:"chat_id" required:"true"`
	UserID    int    `json:"user_id" required:"true"`
	UntilDate int    `json:"until_date"`
}

// KickChatMember creates new Telegram Bot API method kickChatMember
// Use this method to kick a user from a group, a supergroup or a channel. In the case of supergroups and channels, the user will not be able to return to the group on their own using invite links, etc., unless unbanned first. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
func KickChatMember(chatID ChatID, userID int) *KickChatMemberMethod {
	return &KickChatMemberMethod{
		ChatID: chatID,
		UserID: userID,
	}
}

// SetChatID - Unique identifier for the target group or username of the target supergroup or channel (in the format @channelusername)
func (m *KickChatMemberMethod) SetChatID(value ChatID) *KickChatMemberMethod {
	m.ChatID = value
	return m
}

// SetUserID - Unique identifier of the target user
func (m *KickChatMemberMethod) SetUserID(value int) *KickChatMemberMethod {
	m.UserID = value
	return m
}

// SetUntilDate - Date when the user will be unbanned, unix time. If user is banned for more than 366 days or less than 30 seconds from the current time they are considered to be banned forever
func (m *KickChatMemberMethod) SetUntilDate(value int) *KickChatMemberMethod {
	m.UntilDate = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *KickChatMemberMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("kickChatMember", m, &res)
}

// UnbanChatMemberMethod represents Telegram Bot API method unbanChatMember
// Use this method to unban a previously kicked user in a supergroup or channel. The user will not return to the group or channel automatically, but will be able to join via link, etc. The bot must be an administrator for this to work. By default, this method guarantees that after the call the user is not a member of the chat, but will be able to join it. So if the user is a member of the chat they will also be removed from the chat. If you don't want this, use the parameter only_if_banned. Returns True on success.
type UnbanChatMemberMethod struct {
	ChatID       ChatID `json:"chat_id" required:"true"`
	UserID       int    `json:"user_id" required:"true"`
	OnlyIfBanned bool   `json:"only_if_banned"`
}

// UnbanChatMember creates new Telegram Bot API method unbanChatMember
// Use this method to unban a previously kicked user in a supergroup or channel. The user will not return to the group or channel automatically, but will be able to join via link, etc. The bot must be an administrator for this to work. By default, this method guarantees that after the call the user is not a member of the chat, but will be able to join it. So if the user is a member of the chat they will also be removed from the chat. If you don't want this, use the parameter only_if_banned. Returns True on success.
func UnbanChatMember(chatID ChatID, userID int) *UnbanChatMemberMethod {
	return &UnbanChatMemberMethod{
		ChatID: chatID,
		UserID: userID,
	}
}

// SetChatID - Unique identifier for the target group or username of the target supergroup or channel (in the format @username)
func (m *UnbanChatMemberMethod) SetChatID(value ChatID) *UnbanChatMemberMethod {
	m.ChatID = value
	return m
}

// SetUserID - Unique identifier of the target user
func (m *UnbanChatMemberMethod) SetUserID(value int) *UnbanChatMemberMethod {
	m.UserID = value
	return m
}

// SetOnlyIfBanned - Do nothing if the user is not banned
func (m *UnbanChatMemberMethod) SetOnlyIfBanned(value bool) *UnbanChatMemberMethod {
	m.OnlyIfBanned = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *UnbanChatMemberMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("unbanChatMember", m, &res)
}

// RestrictChatMemberMethod represents Telegram Bot API method restrictChatMember
// Use this method to restrict a user in a supergroup. The bot must be an administrator in the supergroup for this to work and must have the appropriate admin rights. Pass True for all permissions to lift restrictions from a user. Returns True on success.
type RestrictChatMemberMethod struct {
	ChatID      ChatID           `json:"chat_id" required:"true"`
	UserID      int              `json:"user_id" required:"true"`
	Permissions *ChatPermissions `json:"permissions" required:"true"`
	UntilDate   int              `json:"until_date"`
}

// RestrictChatMember creates new Telegram Bot API method restrictChatMember
// Use this method to restrict a user in a supergroup. The bot must be an administrator in the supergroup for this to work and must have the appropriate admin rights. Pass True for all permissions to lift restrictions from a user. Returns True on success.
func RestrictChatMember(chatID ChatID, userID int, permissions *ChatPermissions) *RestrictChatMemberMethod {
	return &RestrictChatMemberMethod{
		ChatID:      chatID,
		UserID:      userID,
		Permissions: permissions,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
func (m *RestrictChatMemberMethod) SetChatID(value ChatID) *RestrictChatMemberMethod {
	m.ChatID = value
	return m
}

// SetUserID - Unique identifier of the target user
func (m *RestrictChatMemberMethod) SetUserID(value int) *RestrictChatMemberMethod {
	m.UserID = value
	return m
}

// SetPermissions - A JSON-serialized object for new user permissions
func (m *RestrictChatMemberMethod) SetPermissions(value *ChatPermissions) *RestrictChatMemberMethod {
	m.Permissions = value
	return m
}

// SetUntilDate - Date when restrictions will be lifted for the user, unix time. If user is restricted for more than 366 days or less than 30 seconds from the current time, they are considered to be restricted forever
func (m *RestrictChatMemberMethod) SetUntilDate(value int) *RestrictChatMemberMethod {
	m.UntilDate = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *RestrictChatMemberMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("restrictChatMember", m, &res)
}

// PromoteChatMemberMethod represents Telegram Bot API method promoteChatMember
// Use this method to promote or demote a user in a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Pass False for all boolean parameters to demote a user. Returns True on success.
type PromoteChatMemberMethod struct {
	ChatID             ChatID `json:"chat_id" required:"true"`
	UserID             int    `json:"user_id" required:"true"`
	IsAnonymous        bool   `json:"is_anonymous"`
	CanChangeInfo      bool   `json:"can_change_info"`
	CanPostMessages    bool   `json:"can_post_messages"`
	CanEditMessages    bool   `json:"can_edit_messages"`
	CanDeleteMessages  bool   `json:"can_delete_messages"`
	CanInviteUsers     bool   `json:"can_invite_users"`
	CanRestrictMembers bool   `json:"can_restrict_members"`
	CanPinMessages     bool   `json:"can_pin_messages"`
	CanPromoteMembers  bool   `json:"can_promote_members"`
}

// PromoteChatMember creates new Telegram Bot API method promoteChatMember
// Use this method to promote or demote a user in a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Pass False for all boolean parameters to demote a user. Returns True on success.
func PromoteChatMember(chatID ChatID, userID int) *PromoteChatMemberMethod {
	return &PromoteChatMemberMethod{
		ChatID: chatID,
		UserID: userID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *PromoteChatMemberMethod) SetChatID(value ChatID) *PromoteChatMemberMethod {
	m.ChatID = value
	return m
}

// SetUserID - Unique identifier of the target user
func (m *PromoteChatMemberMethod) SetUserID(value int) *PromoteChatMemberMethod {
	m.UserID = value
	return m
}

// SetIsAnonymous - Pass True, if the administrator's presence in the chat is hidden
func (m *PromoteChatMemberMethod) SetIsAnonymous(value bool) *PromoteChatMemberMethod {
	m.IsAnonymous = value
	return m
}

// SetCanChangeInfo - Pass True, if the administrator can change chat title, photo and other settings
func (m *PromoteChatMemberMethod) SetCanChangeInfo(value bool) *PromoteChatMemberMethod {
	m.CanChangeInfo = value
	return m
}

// SetCanPostMessages - Pass True, if the administrator can create channel posts, channels only
func (m *PromoteChatMemberMethod) SetCanPostMessages(value bool) *PromoteChatMemberMethod {
	m.CanPostMessages = value
	return m
}

// SetCanEditMessages - Pass True, if the administrator can edit messages of other users and can pin messages, channels only
func (m *PromoteChatMemberMethod) SetCanEditMessages(value bool) *PromoteChatMemberMethod {
	m.CanEditMessages = value
	return m
}

// SetCanDeleteMessages - Pass True, if the administrator can delete messages of other users
func (m *PromoteChatMemberMethod) SetCanDeleteMessages(value bool) *PromoteChatMemberMethod {
	m.CanDeleteMessages = value
	return m
}

// SetCanInviteUsers - Pass True, if the administrator can invite new users to the chat
func (m *PromoteChatMemberMethod) SetCanInviteUsers(value bool) *PromoteChatMemberMethod {
	m.CanInviteUsers = value
	return m
}

// SetCanRestrictMembers - Pass True, if the administrator can restrict, ban or unban chat members
func (m *PromoteChatMemberMethod) SetCanRestrictMembers(value bool) *PromoteChatMemberMethod {
	m.CanRestrictMembers = value
	return m
}

// SetCanPinMessages - Pass True, if the administrator can pin messages, supergroups only
func (m *PromoteChatMemberMethod) SetCanPinMessages(value bool) *PromoteChatMemberMethod {
	m.CanPinMessages = value
	return m
}

// SetCanPromoteMembers - Pass True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by him)
func (m *PromoteChatMemberMethod) SetCanPromoteMembers(value bool) *PromoteChatMemberMethod {
	m.CanPromoteMembers = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *PromoteChatMemberMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("promoteChatMember", m, &res)
}

// SetChatAdministratorCustomTitleMethod represents Telegram Bot API method setChatAdministratorCustomTitle
// Use this method to set a custom title for an administrator in a supergroup promoted by the bot. Returns True on success.
type SetChatAdministratorCustomTitleMethod struct {
	ChatID      ChatID `json:"chat_id" required:"true"`
	UserID      int    `json:"user_id" required:"true"`
	CustomTitle string `json:"custom_title" required:"true"`
}

// SetChatAdministratorCustomTitle creates new Telegram Bot API method setChatAdministratorCustomTitle
// Use this method to set a custom title for an administrator in a supergroup promoted by the bot. Returns True on success.
func SetChatAdministratorCustomTitle(chatID ChatID, userID int, customTitle string) *SetChatAdministratorCustomTitleMethod {
	return &SetChatAdministratorCustomTitleMethod{
		ChatID:      chatID,
		UserID:      userID,
		CustomTitle: customTitle,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
func (m *SetChatAdministratorCustomTitleMethod) SetChatID(value ChatID) *SetChatAdministratorCustomTitleMethod {
	m.ChatID = value
	return m
}

// SetUserID - Unique identifier of the target user
func (m *SetChatAdministratorCustomTitleMethod) SetUserID(value int) *SetChatAdministratorCustomTitleMethod {
	m.UserID = value
	return m
}

// SetCustomTitle - New custom title for the administrator; 0-16 characters, emoji are not allowed
func (m *SetChatAdministratorCustomTitleMethod) SetCustomTitle(value string) *SetChatAdministratorCustomTitleMethod {
	m.CustomTitle = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetChatAdministratorCustomTitleMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setChatAdministratorCustomTitle", m, &res)
}

// SetChatPermissionsMethod represents Telegram Bot API method setChatPermissions
// Use this method to set default chat permissions for all members. The bot must be an administrator in the group or a supergroup for this to work and must have the can_restrict_members admin rights. Returns True on success.
type SetChatPermissionsMethod struct {
	ChatID      ChatID           `json:"chat_id" required:"true"`
	Permissions *ChatPermissions `json:"permissions" required:"true"`
}

// SetChatPermissions creates new Telegram Bot API method setChatPermissions
// Use this method to set default chat permissions for all members. The bot must be an administrator in the group or a supergroup for this to work and must have the can_restrict_members admin rights. Returns True on success.
func SetChatPermissions(chatID ChatID, permissions *ChatPermissions) *SetChatPermissionsMethod {
	return &SetChatPermissionsMethod{
		ChatID:      chatID,
		Permissions: permissions,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
func (m *SetChatPermissionsMethod) SetChatID(value ChatID) *SetChatPermissionsMethod {
	m.ChatID = value
	return m
}

// SetPermissions - New default chat permissions
func (m *SetChatPermissionsMethod) SetPermissions(value *ChatPermissions) *SetChatPermissionsMethod {
	m.Permissions = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetChatPermissionsMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setChatPermissions", m, &res)
}

// ExportChatInviteLinkMethod represents Telegram Bot API method exportChatInviteLink
// Use this method to generate a new invite link for a chat; any previously generated link is revoked. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns the new invite link as String on success.
type ExportChatInviteLinkMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// ExportChatInviteLink creates new Telegram Bot API method exportChatInviteLink
// Use this method to generate a new invite link for a chat; any previously generated link is revoked. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns the new invite link as String on success.
func ExportChatInviteLink(chatID ChatID) *ExportChatInviteLinkMethod {
	return &ExportChatInviteLinkMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *ExportChatInviteLinkMethod) SetChatID(value ChatID) *ExportChatInviteLinkMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *ExportChatInviteLinkMethod) Send(client *Client) (res *string, err error) {
	return res, client.request("exportChatInviteLink", m, &res)
}

// SetChatPhotoMethod represents Telegram Bot API method setChatPhoto
// Use this method to set a new profile photo for the chat. Photos can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
type SetChatPhotoMethod struct {
	ChatID ChatID    `json:"chat_id" required:"true"`
	Photo  InputFile `json:"photo" required:"true"`
}

// SetChatPhoto creates new Telegram Bot API method setChatPhoto
// Use this method to set a new profile photo for the chat. Photos can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
func SetChatPhoto(chatID ChatID, photo InputFile) *SetChatPhotoMethod {
	return &SetChatPhotoMethod{
		ChatID: chatID,
		Photo:  photo,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SetChatPhotoMethod) SetChatID(value ChatID) *SetChatPhotoMethod {
	m.ChatID = value
	return m
}

// SetPhoto - New chat photo, uploaded using multipart/form-data
func (m *SetChatPhotoMethod) SetPhoto(value InputFile) *SetChatPhotoMethod {
	m.Photo = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetChatPhotoMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setChatPhoto", m, &res)
}

// DeleteChatPhotoMethod represents Telegram Bot API method deleteChatPhoto
// Use this method to delete a chat photo. Photos can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
type DeleteChatPhotoMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// DeleteChatPhoto creates new Telegram Bot API method deleteChatPhoto
// Use this method to delete a chat photo. Photos can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
func DeleteChatPhoto(chatID ChatID) *DeleteChatPhotoMethod {
	return &DeleteChatPhotoMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *DeleteChatPhotoMethod) SetChatID(value ChatID) *DeleteChatPhotoMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *DeleteChatPhotoMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("deleteChatPhoto", m, &res)
}

// SetChatTitleMethod represents Telegram Bot API method setChatTitle
// Use this method to change the title of a chat. Titles can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
type SetChatTitleMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
	Title  string `json:"title" required:"true"`
}

// SetChatTitle creates new Telegram Bot API method setChatTitle
// Use this method to change the title of a chat. Titles can't be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
func SetChatTitle(chatID ChatID, title string) *SetChatTitleMethod {
	return &SetChatTitleMethod{
		ChatID: chatID,
		Title:  title,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SetChatTitleMethod) SetChatID(value ChatID) *SetChatTitleMethod {
	m.ChatID = value
	return m
}

// SetTitle - New chat title, 1-255 characters
func (m *SetChatTitleMethod) SetTitle(value string) *SetChatTitleMethod {
	m.Title = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetChatTitleMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setChatTitle", m, &res)
}

// SetChatDescriptionMethod represents Telegram Bot API method setChatDescription
// Use this method to change the description of a group, a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
type SetChatDescriptionMethod struct {
	ChatID      ChatID `json:"chat_id" required:"true"`
	Description string `json:"description"`
}

// SetChatDescription creates new Telegram Bot API method setChatDescription
// Use this method to change the description of a group, a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Returns True on success.
func SetChatDescription(chatID ChatID) *SetChatDescriptionMethod {
	return &SetChatDescriptionMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SetChatDescriptionMethod) SetChatID(value ChatID) *SetChatDescriptionMethod {
	m.ChatID = value
	return m
}

// SetDescription - New chat description, 0-255 characters
func (m *SetChatDescriptionMethod) SetDescription(value string) *SetChatDescriptionMethod {
	m.Description = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetChatDescriptionMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setChatDescription", m, &res)
}

// PinChatMessageMethod represents Telegram Bot API method pinChatMessage
// Use this method to add a message to the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' admin right in a supergroup or 'can_edit_messages' admin right in a channel. Returns True on success.
type PinChatMessageMethod struct {
	ChatID              ChatID `json:"chat_id" required:"true"`
	MessageID           int    `json:"message_id" required:"true"`
	DisableNotification bool   `json:"disable_notification"`
}

// PinChatMessage creates new Telegram Bot API method pinChatMessage
// Use this method to add a message to the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' admin right in a supergroup or 'can_edit_messages' admin right in a channel. Returns True on success.
func PinChatMessage(chatID ChatID, messageID int) *PinChatMessageMethod {
	return &PinChatMessageMethod{
		ChatID:    chatID,
		MessageID: messageID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *PinChatMessageMethod) SetChatID(value ChatID) *PinChatMessageMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Identifier of a message to pin
func (m *PinChatMessageMethod) SetMessageID(value int) *PinChatMessageMethod {
	m.MessageID = value
	return m
}

// SetDisableNotification - Pass True, if it is not necessary to send a notification to all chat members about the new pinned message. Notifications are always disabled in channels and private chats.
func (m *PinChatMessageMethod) SetDisableNotification(value bool) *PinChatMessageMethod {
	m.DisableNotification = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *PinChatMessageMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("pinChatMessage", m, &res)
}

// UnpinChatMessageMethod represents Telegram Bot API method unpinChatMessage
// Use this method to remove a message from the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' admin right in a supergroup or 'can_edit_messages' admin right in a channel. Returns True on success.
type UnpinChatMessageMethod struct {
	ChatID    ChatID `json:"chat_id" required:"true"`
	MessageID int    `json:"message_id"`
}

// UnpinChatMessage creates new Telegram Bot API method unpinChatMessage
// Use this method to remove a message from the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' admin right in a supergroup or 'can_edit_messages' admin right in a channel. Returns True on success.
func UnpinChatMessage(chatID ChatID) *UnpinChatMessageMethod {
	return &UnpinChatMessageMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *UnpinChatMessageMethod) SetChatID(value ChatID) *UnpinChatMessageMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Identifier of a message to unpin. If not specified, the most recent pinned message (by sending date) will be unpinned.
func (m *UnpinChatMessageMethod) SetMessageID(value int) *UnpinChatMessageMethod {
	m.MessageID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *UnpinChatMessageMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("unpinChatMessage", m, &res)
}

// UnpinAllChatMessagesMethod represents Telegram Bot API method unpinAllChatMessages
// Use this method to clear the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' admin right in a supergroup or 'can_edit_messages' admin right in a channel. Returns True on success.
type UnpinAllChatMessagesMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// UnpinAllChatMessages creates new Telegram Bot API method unpinAllChatMessages
// Use this method to clear the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' admin right in a supergroup or 'can_edit_messages' admin right in a channel. Returns True on success.
func UnpinAllChatMessages(chatID ChatID) *UnpinAllChatMessagesMethod {
	return &UnpinAllChatMessagesMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *UnpinAllChatMessagesMethod) SetChatID(value ChatID) *UnpinAllChatMessagesMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *UnpinAllChatMessagesMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("unpinAllChatMessages", m, &res)
}

// LeaveChatMethod represents Telegram Bot API method leaveChat
// Use this method for your bot to leave a group, supergroup or channel. Returns True on success.
type LeaveChatMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// LeaveChat creates new Telegram Bot API method leaveChat
// Use this method for your bot to leave a group, supergroup or channel. Returns True on success.
func LeaveChat(chatID ChatID) *LeaveChatMethod {
	return &LeaveChatMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
func (m *LeaveChatMethod) SetChatID(value ChatID) *LeaveChatMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *LeaveChatMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("leaveChat", m, &res)
}

// GetChatMethod represents Telegram Bot API method getChat
// Use this method to get up to date information about the chat (current name of the user for one-on-one conversations, current username of a user, group or channel, etc.). Returns a Chat object on success.
type GetChatMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// GetChat creates new Telegram Bot API method getChat
// Use this method to get up to date information about the chat (current name of the user for one-on-one conversations, current username of a user, group or channel, etc.). Returns a Chat object on success.
func GetChat(chatID ChatID) *GetChatMethod {
	return &GetChatMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
func (m *GetChatMethod) SetChatID(value ChatID) *GetChatMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetChatMethod) Send(client *Client) (res *Chat, err error) {
	return res, client.request("getChat", m, &res)
}

// GetChatAdministratorsMethod represents Telegram Bot API method getChatAdministrators
// Use this method to get a list of administrators in a chat. On success, returns an Array of ChatMember objects that contains information about all chat administrators except other bots. If the chat is a group or a supergroup and no administrators were appointed, only the creator will be returned.
type GetChatAdministratorsMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// GetChatAdministrators creates new Telegram Bot API method getChatAdministrators
// Use this method to get a list of administrators in a chat. On success, returns an Array of ChatMember objects that contains information about all chat administrators except other bots. If the chat is a group or a supergroup and no administrators were appointed, only the creator will be returned.
func GetChatAdministrators(chatID ChatID) *GetChatAdministratorsMethod {
	return &GetChatAdministratorsMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
func (m *GetChatAdministratorsMethod) SetChatID(value ChatID) *GetChatAdministratorsMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetChatAdministratorsMethod) Send(client *Client) (res []*ChatMember, err error) {
	return res, client.request("getChatAdministrators", m, &res)
}

// GetChatMembersCountMethod represents Telegram Bot API method getChatMembersCount
// Use this method to get the number of members in a chat. Returns Int on success.
type GetChatMembersCountMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// GetChatMembersCount creates new Telegram Bot API method getChatMembersCount
// Use this method to get the number of members in a chat. Returns Int on success.
func GetChatMembersCount(chatID ChatID) *GetChatMembersCountMethod {
	return &GetChatMembersCountMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
func (m *GetChatMembersCountMethod) SetChatID(value ChatID) *GetChatMembersCountMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetChatMembersCountMethod) Send(client *Client) (res *int, err error) {
	return res, client.request("getChatMembersCount", m, &res)
}

// GetChatMemberMethod represents Telegram Bot API method getChatMember
// Use this method to get information about a member of a chat. Returns a ChatMember object on success.
type GetChatMemberMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
	UserID int    `json:"user_id" required:"true"`
}

// GetChatMember creates new Telegram Bot API method getChatMember
// Use this method to get information about a member of a chat. Returns a ChatMember object on success.
func GetChatMember(chatID ChatID, userID int) *GetChatMemberMethod {
	return &GetChatMemberMethod{
		ChatID: chatID,
		UserID: userID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup or channel (in the format @channelusername)
func (m *GetChatMemberMethod) SetChatID(value ChatID) *GetChatMemberMethod {
	m.ChatID = value
	return m
}

// SetUserID - Unique identifier of the target user
func (m *GetChatMemberMethod) SetUserID(value int) *GetChatMemberMethod {
	m.UserID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetChatMemberMethod) Send(client *Client) (res *ChatMember, err error) {
	return res, client.request("getChatMember", m, &res)
}

// SetChatStickerSetMethod represents Telegram Bot API method setChatStickerSet
// Use this method to set a new group sticker set for a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method. Returns True on success.
type SetChatStickerSetMethod struct {
	ChatID         ChatID `json:"chat_id" required:"true"`
	StickerSetName string `json:"sticker_set_name" required:"true"`
}

// SetChatStickerSet creates new Telegram Bot API method setChatStickerSet
// Use this method to set a new group sticker set for a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method. Returns True on success.
func SetChatStickerSet(chatID ChatID, stickerSetName string) *SetChatStickerSetMethod {
	return &SetChatStickerSetMethod{
		ChatID:         chatID,
		StickerSetName: stickerSetName,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
func (m *SetChatStickerSetMethod) SetChatID(value ChatID) *SetChatStickerSetMethod {
	m.ChatID = value
	return m
}

// SetStickerSetName - Name of the sticker set to be set as the group sticker set
func (m *SetChatStickerSetMethod) SetStickerSetName(value string) *SetChatStickerSetMethod {
	m.StickerSetName = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetChatStickerSetMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setChatStickerSet", m, &res)
}

// DeleteChatStickerSetMethod represents Telegram Bot API method deleteChatStickerSet
// Use this method to delete a group sticker set from a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method. Returns True on success.
type DeleteChatStickerSetMethod struct {
	ChatID ChatID `json:"chat_id" required:"true"`
}

// DeleteChatStickerSet creates new Telegram Bot API method deleteChatStickerSet
// Use this method to delete a group sticker set from a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate admin rights. Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method. Returns True on success.
func DeleteChatStickerSet(chatID ChatID) *DeleteChatStickerSetMethod {
	return &DeleteChatStickerSetMethod{
		ChatID: chatID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
func (m *DeleteChatStickerSetMethod) SetChatID(value ChatID) *DeleteChatStickerSetMethod {
	m.ChatID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *DeleteChatStickerSetMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("deleteChatStickerSet", m, &res)
}

// AnswerCallbackQueryMethod represents Telegram Bot API method answerCallbackQuery
// Use this method to send answers to callback queries sent from inline keyboards. The answer will be displayed to the user as a notification at the top of the chat screen or as an alert. On success, True is returned.
type AnswerCallbackQueryMethod struct {
	CallbackQueryID string `json:"callback_query_id" required:"true"`
	Text            string `json:"text"`
	ShowAlert       bool   `json:"show_alert"`
	URL             string `json:"url"`
	CacheTime       int    `json:"cache_time"`
}

// AnswerCallbackQuery creates new Telegram Bot API method answerCallbackQuery
// Use this method to send answers to callback queries sent from inline keyboards. The answer will be displayed to the user as a notification at the top of the chat screen or as an alert. On success, True is returned.
func AnswerCallbackQuery(callbackQueryID string) *AnswerCallbackQueryMethod {
	return &AnswerCallbackQueryMethod{
		CallbackQueryID: callbackQueryID,
	}
}

// SetCallbackQueryID - Unique identifier for the query to be answered
func (m *AnswerCallbackQueryMethod) SetCallbackQueryID(value string) *AnswerCallbackQueryMethod {
	m.CallbackQueryID = value
	return m
}

// SetText - Text of the notification. If not specified, nothing will be shown to the user, 0-200 characters
func (m *AnswerCallbackQueryMethod) SetText(value string) *AnswerCallbackQueryMethod {
	m.Text = value
	return m
}

// SetShowAlert - If true, an alert will be shown by the client instead of a notification at the top of the chat screen. Defaults to false.
func (m *AnswerCallbackQueryMethod) SetShowAlert(value bool) *AnswerCallbackQueryMethod {
	m.ShowAlert = value
	return m
}

// SetURL - URL that will be opened by the user's client. If you have created a Game and accepted the conditions via @Botfather, specify the URL that opens your game — note that this will only work if the query comes from a callback_game button.Otherwise, you may use links like t.me/your_bot?start=XXXX that open your bot with a parameter.
func (m *AnswerCallbackQueryMethod) SetURL(value string) *AnswerCallbackQueryMethod {
	m.URL = value
	return m
}

// SetCacheTime - The maximum amount of time in seconds that the result of the callback query may be cached client-side. Telegram apps will support caching starting in version 3.14. Defaults to 0.
func (m *AnswerCallbackQueryMethod) SetCacheTime(value int) *AnswerCallbackQueryMethod {
	m.CacheTime = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *AnswerCallbackQueryMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("answerCallbackQuery", m, &res)
}

// SetMyCommandsMethod represents Telegram Bot API method setMyCommands
// Use this method to change the list of the bot's commands. Returns True on success.
type SetMyCommandsMethod struct {
	Commands []*BotCommand `json:"commands" required:"true"`
}

// SetMyCommands creates new Telegram Bot API method setMyCommands
// Use this method to change the list of the bot's commands. Returns True on success.
func SetMyCommands(commands []*BotCommand) *SetMyCommandsMethod {
	return &SetMyCommandsMethod{
		Commands: commands,
	}
}

// SetCommands - A JSON-serialized list of bot commands to be set as the list of the bot's commands. At most 100 commands can be specified.
func (m *SetMyCommandsMethod) SetCommands(value []*BotCommand) *SetMyCommandsMethod {
	m.Commands = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetMyCommandsMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setMyCommands", m, &res)
}

// GetMyCommandsMethod represents Telegram Bot API method getMyCommands
// Use this method to get the current list of the bot's commands. Requires no parameters. Returns Array of BotCommand on success.
type GetMyCommandsMethod struct {
}

// GetMyCommands creates new Telegram Bot API method getMyCommands
// Use this method to get the current list of the bot's commands. Requires no parameters. Returns Array of BotCommand on success.
func GetMyCommands() *GetMyCommandsMethod {
	return &GetMyCommandsMethod{}
}

// Send is used to send request using Telegram Bot Client
func (m *GetMyCommandsMethod) Send(client *Client) (res []*BotCommand, err error) {
	return res, client.request("getMyCommands", m, &res)
}

// EditMessageTextMethod represents Telegram Bot API method editMessageText
// Use this method to edit text and game messages. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type EditMessageTextMethod struct {
	ChatID                ChatID                `json:"chat_id"`
	MessageID             int                   `json:"message_id"`
	InlineMessageID       string                `json:"inline_message_id"`
	Text                  string                `json:"text" required:"true"`
	ParseMode             string                `json:"parse_mode"`
	Entities              []*MessageEntity      `json:"entities"`
	DisableWebPagePreview bool                  `json:"disable_web_page_preview"`
	ReplyMarkup           *InlineKeyboardMarkup `json:"reply_markup"`
}

// EditMessageText creates new Telegram Bot API method editMessageText
// Use this method to edit text and game messages. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
func EditMessageText(text string) *EditMessageTextMethod {
	return &EditMessageTextMethod{
		Text: text,
	}
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *EditMessageTextMethod) SetChatID(value ChatID) *EditMessageTextMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the message to edit
func (m *EditMessageTextMethod) SetMessageID(value int) *EditMessageTextMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *EditMessageTextMethod) SetInlineMessageID(value string) *EditMessageTextMethod {
	m.InlineMessageID = value
	return m
}

// SetText - New text of the message, 1-4096 characters after entities parsing
func (m *EditMessageTextMethod) SetText(value string) *EditMessageTextMethod {
	m.Text = value
	return m
}

// SetParseMode - Mode for parsing entities in the message text. See formatting options for more details.
func (m *EditMessageTextMethod) SetParseMode(value string) *EditMessageTextMethod {
	m.ParseMode = value
	return m
}

// SetEntities - List of special entities that appear in message text, which can be specified instead of parse_mode
func (m *EditMessageTextMethod) SetEntities(value []*MessageEntity) *EditMessageTextMethod {
	m.Entities = value
	return m
}

// SetDisableWebPagePreview - Disables link previews for links in this message
func (m *EditMessageTextMethod) SetDisableWebPagePreview(value bool) *EditMessageTextMethod {
	m.DisableWebPagePreview = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for an inline keyboard.
func (m *EditMessageTextMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *EditMessageTextMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *EditMessageTextMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("editMessageText", m, &res)
}

// EditMessageCaptionMethod represents Telegram Bot API method editMessageCaption
// Use this method to edit captions of messages. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type EditMessageCaptionMethod struct {
	ChatID          ChatID                `json:"chat_id"`
	MessageID       int                   `json:"message_id"`
	InlineMessageID string                `json:"inline_message_id"`
	Caption         string                `json:"caption"`
	ParseMode       string                `json:"parse_mode"`
	CaptionEntities []*MessageEntity      `json:"caption_entities"`
	ReplyMarkup     *InlineKeyboardMarkup `json:"reply_markup"`
}

// EditMessageCaption creates new Telegram Bot API method editMessageCaption
// Use this method to edit captions of messages. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
func EditMessageCaption() *EditMessageCaptionMethod {
	return &EditMessageCaptionMethod{}
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *EditMessageCaptionMethod) SetChatID(value ChatID) *EditMessageCaptionMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the message to edit
func (m *EditMessageCaptionMethod) SetMessageID(value int) *EditMessageCaptionMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *EditMessageCaptionMethod) SetInlineMessageID(value string) *EditMessageCaptionMethod {
	m.InlineMessageID = value
	return m
}

// SetCaption - New caption of the message, 0-1024 characters after entities parsing
func (m *EditMessageCaptionMethod) SetCaption(value string) *EditMessageCaptionMethod {
	m.Caption = value
	return m
}

// SetParseMode - Mode for parsing entities in the message caption. See formatting options for more details.
func (m *EditMessageCaptionMethod) SetParseMode(value string) *EditMessageCaptionMethod {
	m.ParseMode = value
	return m
}

// SetCaptionEntities - List of special entities that appear in the caption, which can be specified instead of parse_mode
func (m *EditMessageCaptionMethod) SetCaptionEntities(value []*MessageEntity) *EditMessageCaptionMethod {
	m.CaptionEntities = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for an inline keyboard.
func (m *EditMessageCaptionMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *EditMessageCaptionMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *EditMessageCaptionMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("editMessageCaption", m, &res)
}

// EditMessageMediaMethod represents Telegram Bot API method editMessageMedia
// Use this method to edit animation, audio, document, photo, or video messages. If a message is part of a message album, then it can be edited only to an audio for audio albums, only to a document for document albums and to a photo or a video otherwise. When an inline message is edited, a new file can't be uploaded. Use a previously uploaded file via its file_id or specify a URL. On success, if the edited message was sent by the bot, the edited Message is returned, otherwise True is returned.
type EditMessageMediaMethod struct {
	ChatID          ChatID                `json:"chat_id"`
	MessageID       int                   `json:"message_id"`
	InlineMessageID string                `json:"inline_message_id"`
	Media           *InputMedia           `json:"media" required:"true"`
	ReplyMarkup     *InlineKeyboardMarkup `json:"reply_markup"`
}

// EditMessageMedia creates new Telegram Bot API method editMessageMedia
// Use this method to edit animation, audio, document, photo, or video messages. If a message is part of a message album, then it can be edited only to an audio for audio albums, only to a document for document albums and to a photo or a video otherwise. When an inline message is edited, a new file can't be uploaded. Use a previously uploaded file via its file_id or specify a URL. On success, if the edited message was sent by the bot, the edited Message is returned, otherwise True is returned.
func EditMessageMedia(media *InputMedia) *EditMessageMediaMethod {
	return &EditMessageMediaMethod{
		Media: media,
	}
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *EditMessageMediaMethod) SetChatID(value ChatID) *EditMessageMediaMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the message to edit
func (m *EditMessageMediaMethod) SetMessageID(value int) *EditMessageMediaMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *EditMessageMediaMethod) SetInlineMessageID(value string) *EditMessageMediaMethod {
	m.InlineMessageID = value
	return m
}

// SetMedia - A JSON-serialized object for a new media content of the message
func (m *EditMessageMediaMethod) SetMedia(value *InputMedia) *EditMessageMediaMethod {
	m.Media = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for a new inline keyboard.
func (m *EditMessageMediaMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *EditMessageMediaMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *EditMessageMediaMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("editMessageMedia", m, &res)
}

// EditMessageReplyMarkupMethod represents Telegram Bot API method editMessageReplyMarkup
// Use this method to edit only the reply markup of messages. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
type EditMessageReplyMarkupMethod struct {
	ChatID          ChatID                `json:"chat_id"`
	MessageID       int                   `json:"message_id"`
	InlineMessageID string                `json:"inline_message_id"`
	ReplyMarkup     *InlineKeyboardMarkup `json:"reply_markup"`
}

// EditMessageReplyMarkup creates new Telegram Bot API method editMessageReplyMarkup
// Use this method to edit only the reply markup of messages. On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
func EditMessageReplyMarkup() *EditMessageReplyMarkupMethod {
	return &EditMessageReplyMarkupMethod{}
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *EditMessageReplyMarkupMethod) SetChatID(value ChatID) *EditMessageReplyMarkupMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the message to edit
func (m *EditMessageReplyMarkupMethod) SetMessageID(value int) *EditMessageReplyMarkupMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *EditMessageReplyMarkupMethod) SetInlineMessageID(value string) *EditMessageReplyMarkupMethod {
	m.InlineMessageID = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for an inline keyboard.
func (m *EditMessageReplyMarkupMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *EditMessageReplyMarkupMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *EditMessageReplyMarkupMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("editMessageReplyMarkup", m, &res)
}

// StopPollMethod represents Telegram Bot API method stopPoll
// Use this method to stop a poll which was sent by the bot. On success, the stopped Poll with the final results is returned.
type StopPollMethod struct {
	ChatID      ChatID                `json:"chat_id" required:"true"`
	MessageID   int                   `json:"message_id" required:"true"`
	ReplyMarkup *InlineKeyboardMarkup `json:"reply_markup"`
}

// StopPoll creates new Telegram Bot API method stopPoll
// Use this method to stop a poll which was sent by the bot. On success, the stopped Poll with the final results is returned.
func StopPoll(chatID ChatID, messageID int) *StopPollMethod {
	return &StopPollMethod{
		ChatID:    chatID,
		MessageID: messageID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *StopPollMethod) SetChatID(value ChatID) *StopPollMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Identifier of the original message with the poll
func (m *StopPollMethod) SetMessageID(value int) *StopPollMethod {
	m.MessageID = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for a new message inline keyboard.
func (m *StopPollMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *StopPollMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *StopPollMethod) Send(client *Client) (res *Poll, err error) {
	return res, client.request("stopPoll", m, &res)
}

// DeleteMessageMethod represents Telegram Bot API method deleteMessage
// Use this method to delete a message, including service messages, with the following limitations:- A message can only be deleted if it was sent less than 48 hours ago.- A dice message in a private chat can only be deleted if it was sent more than 24 hours ago.- Bots can delete outgoing messages in private chats, groups, and supergroups.- Bots can delete incoming messages in private chats.- Bots granted can_post_messages permissions can delete outgoing messages in channels.- If the bot is an administrator of a group, it can delete any message there.- If the bot has can_delete_messages permission in a supergroup or a channel, it can delete any message there.Returns True on success.
type DeleteMessageMethod struct {
	ChatID    ChatID `json:"chat_id" required:"true"`
	MessageID int    `json:"message_id" required:"true"`
}

// DeleteMessage creates new Telegram Bot API method deleteMessage
// Use this method to delete a message, including service messages, with the following limitations:- A message can only be deleted if it was sent less than 48 hours ago.- A dice message in a private chat can only be deleted if it was sent more than 24 hours ago.- Bots can delete outgoing messages in private chats, groups, and supergroups.- Bots can delete incoming messages in private chats.- Bots granted can_post_messages permissions can delete outgoing messages in channels.- If the bot is an administrator of a group, it can delete any message there.- If the bot has can_delete_messages permission in a supergroup or a channel, it can delete any message there.Returns True on success.
func DeleteMessage(chatID ChatID, messageID int) *DeleteMessageMethod {
	return &DeleteMessageMethod{
		ChatID:    chatID,
		MessageID: messageID,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *DeleteMessageMethod) SetChatID(value ChatID) *DeleteMessageMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Identifier of the message to delete
func (m *DeleteMessageMethod) SetMessageID(value int) *DeleteMessageMethod {
	m.MessageID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *DeleteMessageMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("deleteMessage", m, &res)
}

// SendStickerMethod represents Telegram Bot API method sendSticker
// Use this method to send static .WEBP or animated .TGS stickers. On success, the sent Message is returned.
type SendStickerMethod struct {
	ChatID                   ChatID      `json:"chat_id" required:"true"`
	Sticker                  InputFile   `json:"sticker" required:"true"`
	DisableNotification      bool        `json:"disable_notification"`
	ReplyToMessageID         int         `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool        `json:"allow_sending_without_reply"`
	ReplyMarkup              ReplyMarkup `json:"reply_markup"`
}

// SendSticker creates new Telegram Bot API method sendSticker
// Use this method to send static .WEBP or animated .TGS stickers. On success, the sent Message is returned.
func SendSticker(chatID ChatID, sticker InputFile) *SendStickerMethod {
	return &SendStickerMethod{
		ChatID:  chatID,
		Sticker: sticker,
	}
}

// SetChatID - Unique identifier for the target chat or username of the target channel (in the format @channelusername)
func (m *SendStickerMethod) SetChatID(value ChatID) *SendStickerMethod {
	m.ChatID = value
	return m
}

// SetSticker - Sticker to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a .WEBP file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
func (m *SendStickerMethod) SetSticker(value InputFile) *SendStickerMethod {
	m.Sticker = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendStickerMethod) SetDisableNotification(value bool) *SendStickerMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendStickerMethod) SetReplyToMessageID(value int) *SendStickerMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendStickerMethod) SetAllowSendingWithoutReply(value bool) *SendStickerMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
func (m *SendStickerMethod) SetReplyMarkup(value ReplyMarkup) *SendStickerMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendStickerMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendSticker", m, &res)
}

// GetStickerSetMethod represents Telegram Bot API method getStickerSet
// Use this method to get a sticker set. On success, a StickerSet object is returned.
type GetStickerSetMethod struct {
	Name string `json:"name" required:"true"`
}

// GetStickerSet creates new Telegram Bot API method getStickerSet
// Use this method to get a sticker set. On success, a StickerSet object is returned.
func GetStickerSet(name string) *GetStickerSetMethod {
	return &GetStickerSetMethod{
		Name: name,
	}
}

// SetName - Name of the sticker set
func (m *GetStickerSetMethod) SetName(value string) *GetStickerSetMethod {
	m.Name = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetStickerSetMethod) Send(client *Client) (res *StickerSet, err error) {
	return res, client.request("getStickerSet", m, &res)
}

// UploadStickerFileMethod represents Telegram Bot API method uploadStickerFile
// Use this method to upload a .PNG file with a sticker for later use in createNewStickerSet and addStickerToSet methods (can be used multiple times). Returns the uploaded File on success.
type UploadStickerFileMethod struct {
	UserID     int       `json:"user_id" required:"true"`
	PngSticker InputFile `json:"png_sticker" required:"true"`
}

// UploadStickerFile creates new Telegram Bot API method uploadStickerFile
// Use this method to upload a .PNG file with a sticker for later use in createNewStickerSet and addStickerToSet methods (can be used multiple times). Returns the uploaded File on success.
func UploadStickerFile(userID int, pngSticker InputFile) *UploadStickerFileMethod {
	return &UploadStickerFileMethod{
		UserID:     userID,
		PngSticker: pngSticker,
	}
}

// SetUserID - User identifier of sticker file owner
func (m *UploadStickerFileMethod) SetUserID(value int) *UploadStickerFileMethod {
	m.UserID = value
	return m
}

// SetPngSticker - PNG image with the sticker, must be up to 512 kilobytes in size, dimensions must not exceed 512px, and either width or height must be exactly 512px. More info on Sending Files »
func (m *UploadStickerFileMethod) SetPngSticker(value InputFile) *UploadStickerFileMethod {
	m.PngSticker = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *UploadStickerFileMethod) Send(client *Client) (res *File, err error) {
	return res, client.request("uploadStickerFile", m, &res)
}

// CreateNewStickerSetMethod represents Telegram Bot API method createNewStickerSet
// Use this method to create a new sticker set owned by a user. The bot will be able to edit the sticker set thus created. You must use exactly one of the fields png_sticker or tgs_sticker. Returns True on success.
type CreateNewStickerSetMethod struct {
	UserID        int           `json:"user_id" required:"true"`
	Name          string        `json:"name" required:"true"`
	Title         string        `json:"title" required:"true"`
	PngSticker    InputFile     `json:"png_sticker"`
	TgsSticker    InputFile     `json:"tgs_sticker"`
	Emojis        string        `json:"emojis" required:"true"`
	ContainsMasks bool          `json:"contains_masks"`
	MaskPosition  *MaskPosition `json:"mask_position"`
}

// CreateNewStickerSet creates new Telegram Bot API method createNewStickerSet
// Use this method to create a new sticker set owned by a user. The bot will be able to edit the sticker set thus created. You must use exactly one of the fields png_sticker or tgs_sticker. Returns True on success.
func CreateNewStickerSet(userID int, name string, title string, emojis string) *CreateNewStickerSetMethod {
	return &CreateNewStickerSetMethod{
		UserID: userID,
		Name:   name,
		Title:  title,
		Emojis: emojis,
	}
}

// SetUserID - User identifier of created sticker set owner
func (m *CreateNewStickerSetMethod) SetUserID(value int) *CreateNewStickerSetMethod {
	m.UserID = value
	return m
}

// SetName - Short name of sticker set, to be used in t.me/addstickers/ URLs (e.g., animals). Can contain only english letters, digits and underscores. Must begin with a letter, can't contain consecutive underscores and must end in “_by_<bot username>”. <bot_username> is case insensitive. 1-64 characters.
func (m *CreateNewStickerSetMethod) SetName(value string) *CreateNewStickerSetMethod {
	m.Name = value
	return m
}

// SetTitle - Sticker set title, 1-64 characters
func (m *CreateNewStickerSetMethod) SetTitle(value string) *CreateNewStickerSetMethod {
	m.Title = value
	return m
}

// SetPngSticker - PNG image with the sticker, must be up to 512 kilobytes in size, dimensions must not exceed 512px, and either width or height must be exactly 512px. Pass a file_id as a String to send a file that already exists on the Telegram servers, pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
func (m *CreateNewStickerSetMethod) SetPngSticker(value InputFile) *CreateNewStickerSetMethod {
	m.PngSticker = value
	return m
}

// SetTgsSticker - TGS animation with the sticker, uploaded using multipart/form-data. See https://core.telegram.org/animated_stickers#technical-requirements for technical requirements
func (m *CreateNewStickerSetMethod) SetTgsSticker(value InputFile) *CreateNewStickerSetMethod {
	m.TgsSticker = value
	return m
}

// SetEmojis - One or more emoji corresponding to the sticker
func (m *CreateNewStickerSetMethod) SetEmojis(value string) *CreateNewStickerSetMethod {
	m.Emojis = value
	return m
}

// SetContainsMasks - Pass True, if a set of mask stickers should be created
func (m *CreateNewStickerSetMethod) SetContainsMasks(value bool) *CreateNewStickerSetMethod {
	m.ContainsMasks = value
	return m
}

// SetMaskPosition - A JSON-serialized object for position where the mask should be placed on faces
func (m *CreateNewStickerSetMethod) SetMaskPosition(value *MaskPosition) *CreateNewStickerSetMethod {
	m.MaskPosition = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *CreateNewStickerSetMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("createNewStickerSet", m, &res)
}

// AddStickerToSetMethod represents Telegram Bot API method addStickerToSet
// Use this method to add a new sticker to a set created by the bot. You must use exactly one of the fields png_sticker or tgs_sticker. Animated stickers can be added to animated sticker sets and only to them. Animated sticker sets can have up to 50 stickers. Static sticker sets can have up to 120 stickers. Returns True on success.
type AddStickerToSetMethod struct {
	UserID       int           `json:"user_id" required:"true"`
	Name         string        `json:"name" required:"true"`
	PngSticker   InputFile     `json:"png_sticker"`
	TgsSticker   InputFile     `json:"tgs_sticker"`
	Emojis       string        `json:"emojis" required:"true"`
	MaskPosition *MaskPosition `json:"mask_position"`
}

// AddStickerToSet creates new Telegram Bot API method addStickerToSet
// Use this method to add a new sticker to a set created by the bot. You must use exactly one of the fields png_sticker or tgs_sticker. Animated stickers can be added to animated sticker sets and only to them. Animated sticker sets can have up to 50 stickers. Static sticker sets can have up to 120 stickers. Returns True on success.
func AddStickerToSet(userID int, name string, emojis string) *AddStickerToSetMethod {
	return &AddStickerToSetMethod{
		UserID: userID,
		Name:   name,
		Emojis: emojis,
	}
}

// SetUserID - User identifier of sticker set owner
func (m *AddStickerToSetMethod) SetUserID(value int) *AddStickerToSetMethod {
	m.UserID = value
	return m
}

// SetName - Sticker set name
func (m *AddStickerToSetMethod) SetName(value string) *AddStickerToSetMethod {
	m.Name = value
	return m
}

// SetPngSticker - PNG image with the sticker, must be up to 512 kilobytes in size, dimensions must not exceed 512px, and either width or height must be exactly 512px. Pass a file_id as a String to send a file that already exists on the Telegram servers, pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files »
func (m *AddStickerToSetMethod) SetPngSticker(value InputFile) *AddStickerToSetMethod {
	m.PngSticker = value
	return m
}

// SetTgsSticker - TGS animation with the sticker, uploaded using multipart/form-data. See https://core.telegram.org/animated_stickers#technical-requirements for technical requirements
func (m *AddStickerToSetMethod) SetTgsSticker(value InputFile) *AddStickerToSetMethod {
	m.TgsSticker = value
	return m
}

// SetEmojis - One or more emoji corresponding to the sticker
func (m *AddStickerToSetMethod) SetEmojis(value string) *AddStickerToSetMethod {
	m.Emojis = value
	return m
}

// SetMaskPosition - A JSON-serialized object for position where the mask should be placed on faces
func (m *AddStickerToSetMethod) SetMaskPosition(value *MaskPosition) *AddStickerToSetMethod {
	m.MaskPosition = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *AddStickerToSetMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("addStickerToSet", m, &res)
}

// SetStickerPositionInSetMethod represents Telegram Bot API method setStickerPositionInSet
// Use this method to move a sticker in a set created by the bot to a specific position. Returns True on success.
type SetStickerPositionInSetMethod struct {
	Sticker  string `json:"sticker" required:"true"`
	Position int    `json:"position" required:"true"`
}

// SetStickerPositionInSet creates new Telegram Bot API method setStickerPositionInSet
// Use this method to move a sticker in a set created by the bot to a specific position. Returns True on success.
func SetStickerPositionInSet(sticker string, position int) *SetStickerPositionInSetMethod {
	return &SetStickerPositionInSetMethod{
		Sticker:  sticker,
		Position: position,
	}
}

// SetSticker - File identifier of the sticker
func (m *SetStickerPositionInSetMethod) SetSticker(value string) *SetStickerPositionInSetMethod {
	m.Sticker = value
	return m
}

// SetPosition - New sticker position in the set, zero-based
func (m *SetStickerPositionInSetMethod) SetPosition(value int) *SetStickerPositionInSetMethod {
	m.Position = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetStickerPositionInSetMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setStickerPositionInSet", m, &res)
}

// DeleteStickerFromSetMethod represents Telegram Bot API method deleteStickerFromSet
// Use this method to delete a sticker from a set created by the bot. Returns True on success.
type DeleteStickerFromSetMethod struct {
	Sticker string `json:"sticker" required:"true"`
}

// DeleteStickerFromSet creates new Telegram Bot API method deleteStickerFromSet
// Use this method to delete a sticker from a set created by the bot. Returns True on success.
func DeleteStickerFromSet(sticker string) *DeleteStickerFromSetMethod {
	return &DeleteStickerFromSetMethod{
		Sticker: sticker,
	}
}

// SetSticker - File identifier of the sticker
func (m *DeleteStickerFromSetMethod) SetSticker(value string) *DeleteStickerFromSetMethod {
	m.Sticker = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *DeleteStickerFromSetMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("deleteStickerFromSet", m, &res)
}

// SetStickerSetThumbMethod represents Telegram Bot API method setStickerSetThumb
// Use this method to set the thumbnail of a sticker set. Animated thumbnails can be set for animated sticker sets only. Returns True on success.
type SetStickerSetThumbMethod struct {
	Name   string    `json:"name" required:"true"`
	UserID int       `json:"user_id" required:"true"`
	Thumb  InputFile `json:"thumb"`
}

// SetStickerSetThumb creates new Telegram Bot API method setStickerSetThumb
// Use this method to set the thumbnail of a sticker set. Animated thumbnails can be set for animated sticker sets only. Returns True on success.
func SetStickerSetThumb(name string, userID int) *SetStickerSetThumbMethod {
	return &SetStickerSetThumbMethod{
		Name:   name,
		UserID: userID,
	}
}

// SetName - Sticker set name
func (m *SetStickerSetThumbMethod) SetName(value string) *SetStickerSetThumbMethod {
	m.Name = value
	return m
}

// SetUserID - User identifier of the sticker set owner
func (m *SetStickerSetThumbMethod) SetUserID(value int) *SetStickerSetThumbMethod {
	m.UserID = value
	return m
}

// SetThumb - A PNG image with the thumbnail, must be up to 128 kilobytes in size and have width and height exactly 100px, or a TGS animation with the thumbnail up to 32 kilobytes in size; see https://core.telegram.org/animated_stickers#technical-requirements for animated sticker technical requirements. Pass a file_id as a String to send a file that already exists on the Telegram servers, pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More info on Sending Files ». Animated sticker set thumbnail can't be uploaded via HTTP URL.
func (m *SetStickerSetThumbMethod) SetThumb(value InputFile) *SetStickerSetThumbMethod {
	m.Thumb = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetStickerSetThumbMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setStickerSetThumb", m, &res)
}

// AnswerInlineQueryMethod represents Telegram Bot API method answerInlineQuery
// Use this method to send answers to an inline query. On success, True is returned.No more than 50 results per query are allowed.
type AnswerInlineQueryMethod struct {
	InlineQueryID     string               `json:"inline_query_id" required:"true"`
	Results           []*InlineQueryResult `json:"results" required:"true"`
	CacheTime         int                  `json:"cache_time"`
	IsPersonal        bool                 `json:"is_personal"`
	NextOffset        string               `json:"next_offset"`
	SwitchPmText      string               `json:"switch_pm_text"`
	SwitchPmParameter string               `json:"switch_pm_parameter"`
}

// AnswerInlineQuery creates new Telegram Bot API method answerInlineQuery
// Use this method to send answers to an inline query. On success, True is returned.No more than 50 results per query are allowed.
func AnswerInlineQuery(inlineQueryID string, results []*InlineQueryResult) *AnswerInlineQueryMethod {
	return &AnswerInlineQueryMethod{
		InlineQueryID: inlineQueryID,
		Results:       results,
	}
}

// SetInlineQueryID - Unique identifier for the answered query
func (m *AnswerInlineQueryMethod) SetInlineQueryID(value string) *AnswerInlineQueryMethod {
	m.InlineQueryID = value
	return m
}

// SetResults - A JSON-serialized array of results for the inline query
func (m *AnswerInlineQueryMethod) SetResults(value []*InlineQueryResult) *AnswerInlineQueryMethod {
	m.Results = value
	return m
}

// SetCacheTime - The maximum amount of time in seconds that the result of the inline query may be cached on the server. Defaults to 300.
func (m *AnswerInlineQueryMethod) SetCacheTime(value int) *AnswerInlineQueryMethod {
	m.CacheTime = value
	return m
}

// SetIsPersonal - Pass True, if results may be cached on the server side only for the user that sent the query. By default, results may be returned to any user who sends the same query
func (m *AnswerInlineQueryMethod) SetIsPersonal(value bool) *AnswerInlineQueryMethod {
	m.IsPersonal = value
	return m
}

// SetNextOffset - Pass the offset that a client should send in the next query with the same text to receive more results. Pass an empty string if there are no more results or if you don't support pagination. Offset length can't exceed 64 bytes.
func (m *AnswerInlineQueryMethod) SetNextOffset(value string) *AnswerInlineQueryMethod {
	m.NextOffset = value
	return m
}

// SetSwitchPmText - If passed, clients will display a button with specified text that switches the user to a private chat with the bot and sends the bot a start message with the parameter switch_pm_parameter
func (m *AnswerInlineQueryMethod) SetSwitchPmText(value string) *AnswerInlineQueryMethod {
	m.SwitchPmText = value
	return m
}

// SetSwitchPmParameter - Deep-linking parameter for the /start message sent to the bot when user presses the switch button. 1-64 characters, only A-Z, a-z, 0-9, _ and - are allowed.Example: An inline bot that sends YouTube videos can ask the user to connect the bot to their YouTube account to adapt search results accordingly. To do this, it displays a 'Connect your YouTube account' button above the results, or even before showing any. The user presses the button, switches to a private chat with the bot and, in doing so, passes a start parameter that instructs the bot to return an oauth link. Once done, the bot can offer a switch_inline button so that the user can easily return to the chat where they wanted to use the bot's inline capabilities.
func (m *AnswerInlineQueryMethod) SetSwitchPmParameter(value string) *AnswerInlineQueryMethod {
	m.SwitchPmParameter = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *AnswerInlineQueryMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("answerInlineQuery", m, &res)
}

// SendInvoiceMethod represents Telegram Bot API method sendInvoice
// Use this method to send invoices. On success, the sent Message is returned.
type SendInvoiceMethod struct {
	ChatID                    int                   `json:"chat_id" required:"true"`
	Title                     string                `json:"title" required:"true"`
	Description               string                `json:"description" required:"true"`
	Payload                   string                `json:"payload" required:"true"`
	ProviderToken             string                `json:"provider_token" required:"true"`
	StartParameter            string                `json:"start_parameter" required:"true"`
	Currency                  string                `json:"currency" required:"true"`
	Prices                    []*LabeledPrice       `json:"prices" required:"true"`
	ProviderData              string                `json:"provider_data"`
	PhotoURL                  string                `json:"photo_url"`
	PhotoSize                 int                   `json:"photo_size"`
	PhotoWidth                int                   `json:"photo_width"`
	PhotoHeight               int                   `json:"photo_height"`
	NeedName                  bool                  `json:"need_name"`
	NeedPhoneNumber           bool                  `json:"need_phone_number"`
	NeedEmail                 bool                  `json:"need_email"`
	NeedShippingAddress       bool                  `json:"need_shipping_address"`
	SendPhoneNumberToProvider bool                  `json:"send_phone_number_to_provider"`
	SendEmailToProvider       bool                  `json:"send_email_to_provider"`
	IsFlexible                bool                  `json:"is_flexible"`
	DisableNotification       bool                  `json:"disable_notification"`
	ReplyToMessageID          int                   `json:"reply_to_message_id"`
	AllowSendingWithoutReply  bool                  `json:"allow_sending_without_reply"`
	ReplyMarkup               *InlineKeyboardMarkup `json:"reply_markup"`
}

// SendInvoice creates new Telegram Bot API method sendInvoice
// Use this method to send invoices. On success, the sent Message is returned.
func SendInvoice(chatID int, title string, description string, payload string, providerToken string, startParameter string, currency string, prices []*LabeledPrice) *SendInvoiceMethod {
	return &SendInvoiceMethod{
		ChatID:         chatID,
		Title:          title,
		Description:    description,
		Payload:        payload,
		ProviderToken:  providerToken,
		StartParameter: startParameter,
		Currency:       currency,
		Prices:         prices,
	}
}

// SetChatID - Unique identifier for the target private chat
func (m *SendInvoiceMethod) SetChatID(value int) *SendInvoiceMethod {
	m.ChatID = value
	return m
}

// SetTitle - Product name, 1-32 characters
func (m *SendInvoiceMethod) SetTitle(value string) *SendInvoiceMethod {
	m.Title = value
	return m
}

// SetDescription - Product description, 1-255 characters
func (m *SendInvoiceMethod) SetDescription(value string) *SendInvoiceMethod {
	m.Description = value
	return m
}

// SetPayload - Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
func (m *SendInvoiceMethod) SetPayload(value string) *SendInvoiceMethod {
	m.Payload = value
	return m
}

// SetProviderToken - Payments provider token, obtained via Botfather
func (m *SendInvoiceMethod) SetProviderToken(value string) *SendInvoiceMethod {
	m.ProviderToken = value
	return m
}

// SetStartParameter - Unique deep-linking parameter that can be used to generate this invoice when used as a start parameter
func (m *SendInvoiceMethod) SetStartParameter(value string) *SendInvoiceMethod {
	m.StartParameter = value
	return m
}

// SetCurrency - Three-letter ISO 4217 currency code, see more on currencies
func (m *SendInvoiceMethod) SetCurrency(value string) *SendInvoiceMethod {
	m.Currency = value
	return m
}

// SetPrices - Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
func (m *SendInvoiceMethod) SetPrices(value []*LabeledPrice) *SendInvoiceMethod {
	m.Prices = value
	return m
}

// SetProviderData - A JSON-serialized data about the invoice, which will be shared with the payment provider. A detailed description of required fields should be provided by the payment provider.
func (m *SendInvoiceMethod) SetProviderData(value string) *SendInvoiceMethod {
	m.ProviderData = value
	return m
}

// SetPhotoURL - URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service. People like it better when they see what they are paying for.
func (m *SendInvoiceMethod) SetPhotoURL(value string) *SendInvoiceMethod {
	m.PhotoURL = value
	return m
}

// SetPhotoSize - Photo size
func (m *SendInvoiceMethod) SetPhotoSize(value int) *SendInvoiceMethod {
	m.PhotoSize = value
	return m
}

// SetPhotoWidth - Photo width
func (m *SendInvoiceMethod) SetPhotoWidth(value int) *SendInvoiceMethod {
	m.PhotoWidth = value
	return m
}

// SetPhotoHeight - Photo height
func (m *SendInvoiceMethod) SetPhotoHeight(value int) *SendInvoiceMethod {
	m.PhotoHeight = value
	return m
}

// SetNeedName - Pass True, if you require the user's full name to complete the order
func (m *SendInvoiceMethod) SetNeedName(value bool) *SendInvoiceMethod {
	m.NeedName = value
	return m
}

// SetNeedPhoneNumber - Pass True, if you require the user's phone number to complete the order
func (m *SendInvoiceMethod) SetNeedPhoneNumber(value bool) *SendInvoiceMethod {
	m.NeedPhoneNumber = value
	return m
}

// SetNeedEmail - Pass True, if you require the user's email address to complete the order
func (m *SendInvoiceMethod) SetNeedEmail(value bool) *SendInvoiceMethod {
	m.NeedEmail = value
	return m
}

// SetNeedShippingAddress - Pass True, if you require the user's shipping address to complete the order
func (m *SendInvoiceMethod) SetNeedShippingAddress(value bool) *SendInvoiceMethod {
	m.NeedShippingAddress = value
	return m
}

// SetSendPhoneNumberToProvider - Pass True, if user's phone number should be sent to provider
func (m *SendInvoiceMethod) SetSendPhoneNumberToProvider(value bool) *SendInvoiceMethod {
	m.SendPhoneNumberToProvider = value
	return m
}

// SetSendEmailToProvider - Pass True, if user's email address should be sent to provider
func (m *SendInvoiceMethod) SetSendEmailToProvider(value bool) *SendInvoiceMethod {
	m.SendEmailToProvider = value
	return m
}

// SetIsFlexible - Pass True, if the final price depends on the shipping method
func (m *SendInvoiceMethod) SetIsFlexible(value bool) *SendInvoiceMethod {
	m.IsFlexible = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendInvoiceMethod) SetDisableNotification(value bool) *SendInvoiceMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendInvoiceMethod) SetReplyToMessageID(value int) *SendInvoiceMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendInvoiceMethod) SetAllowSendingWithoutReply(value bool) *SendInvoiceMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for an inline keyboard. If empty, one 'Pay total price' button will be shown. If not empty, the first button must be a Pay button.
func (m *SendInvoiceMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *SendInvoiceMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendInvoiceMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendInvoice", m, &res)
}

// AnswerShippingQueryMethod represents Telegram Bot API method answerShippingQuery
// If you sent an invoice requesting a shipping address and the parameter is_flexible was specified, the Bot API will send an Update with a shipping_query field to the bot. Use this method to reply to shipping queries. On success, True is returned.
type AnswerShippingQueryMethod struct {
	ShippingQueryID string            `json:"shipping_query_id" required:"true"`
	Ok              bool              `json:"ok" required:"true"`
	ShippingOptions []*ShippingOption `json:"shipping_options"`
	ErrorMessage    string            `json:"error_message"`
}

// AnswerShippingQuery creates new Telegram Bot API method answerShippingQuery
// If you sent an invoice requesting a shipping address and the parameter is_flexible was specified, the Bot API will send an Update with a shipping_query field to the bot. Use this method to reply to shipping queries. On success, True is returned.
func AnswerShippingQuery(shippingQueryID string, ok bool) *AnswerShippingQueryMethod {
	return &AnswerShippingQueryMethod{
		ShippingQueryID: shippingQueryID,
		Ok:              ok,
	}
}

// SetShippingQueryID - Unique identifier for the query to be answered
func (m *AnswerShippingQueryMethod) SetShippingQueryID(value string) *AnswerShippingQueryMethod {
	m.ShippingQueryID = value
	return m
}

// SetOk - Specify True if delivery to the specified address is possible and False if there are any problems (for example, if delivery to the specified address is not possible)
func (m *AnswerShippingQueryMethod) SetOk(value bool) *AnswerShippingQueryMethod {
	m.Ok = value
	return m
}

// SetShippingOptions - Required if ok is True. A JSON-serialized array of available shipping options.
func (m *AnswerShippingQueryMethod) SetShippingOptions(value []*ShippingOption) *AnswerShippingQueryMethod {
	m.ShippingOptions = value
	return m
}

// SetErrorMessage - Required if ok is False. Error message in human readable form that explains why it is impossible to complete the order (e.g. "Sorry, delivery to your desired address is unavailable'). Telegram will display this message to the user.
func (m *AnswerShippingQueryMethod) SetErrorMessage(value string) *AnswerShippingQueryMethod {
	m.ErrorMessage = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *AnswerShippingQueryMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("answerShippingQuery", m, &res)
}

// AnswerPreCheckoutQueryMethod represents Telegram Bot API method answerPreCheckoutQuery
// Once the user has confirmed their payment and shipping details, the Bot API sends the final confirmation in the form of an Update with the field pre_checkout_query. Use this method to respond to such pre-checkout queries. On success, True is returned. Note: The Bot API must receive an answer within 10 seconds after the pre-checkout query was sent.
type AnswerPreCheckoutQueryMethod struct {
	PreCheckoutQueryID string `json:"pre_checkout_query_id" required:"true"`
	Ok                 bool   `json:"ok" required:"true"`
	ErrorMessage       string `json:"error_message"`
}

// AnswerPreCheckoutQuery creates new Telegram Bot API method answerPreCheckoutQuery
// Once the user has confirmed their payment and shipping details, the Bot API sends the final confirmation in the form of an Update with the field pre_checkout_query. Use this method to respond to such pre-checkout queries. On success, True is returned. Note: The Bot API must receive an answer within 10 seconds after the pre-checkout query was sent.
func AnswerPreCheckoutQuery(preCheckoutQueryID string, ok bool) *AnswerPreCheckoutQueryMethod {
	return &AnswerPreCheckoutQueryMethod{
		PreCheckoutQueryID: preCheckoutQueryID,
		Ok:                 ok,
	}
}

// SetPreCheckoutQueryID - Unique identifier for the query to be answered
func (m *AnswerPreCheckoutQueryMethod) SetPreCheckoutQueryID(value string) *AnswerPreCheckoutQueryMethod {
	m.PreCheckoutQueryID = value
	return m
}

// SetOk - Specify True if everything is alright (goods are available, etc.) and the bot is ready to proceed with the order. Use False if there are any problems.
func (m *AnswerPreCheckoutQueryMethod) SetOk(value bool) *AnswerPreCheckoutQueryMethod {
	m.Ok = value
	return m
}

// SetErrorMessage - Required if ok is False. Error message in human readable form that explains the reason for failure to proceed with the checkout (e.g. "Sorry, somebody just bought the last of our amazing black T-shirts while you were busy filling out your payment details. Please choose a different color or garment!"). Telegram will display this message to the user.
func (m *AnswerPreCheckoutQueryMethod) SetErrorMessage(value string) *AnswerPreCheckoutQueryMethod {
	m.ErrorMessage = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *AnswerPreCheckoutQueryMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("answerPreCheckoutQuery", m, &res)
}

// SetPassportDataErrorsMethod represents Telegram Bot API method setPassportDataErrors
// Informs a user that some of the Telegram Passport elements they provided contains errors. The user will not be able to re-submit their Passport to you until the errors are fixed (the contents of the field for which you returned the error must change). Returns True on success.
type SetPassportDataErrorsMethod struct {
	UserID int                     `json:"user_id" required:"true"`
	Errors []*PassportElementError `json:"errors" required:"true"`
}

// SetPassportDataErrors creates new Telegram Bot API method setPassportDataErrors
// Informs a user that some of the Telegram Passport elements they provided contains errors. The user will not be able to re-submit their Passport to you until the errors are fixed (the contents of the field for which you returned the error must change). Returns True on success.
func SetPassportDataErrors(userID int, errors []*PassportElementError) *SetPassportDataErrorsMethod {
	return &SetPassportDataErrorsMethod{
		UserID: userID,
		Errors: errors,
	}
}

// SetUserID - User identifier
func (m *SetPassportDataErrorsMethod) SetUserID(value int) *SetPassportDataErrorsMethod {
	m.UserID = value
	return m
}

// SetErrors - A JSON-serialized array describing the errors
func (m *SetPassportDataErrorsMethod) SetErrors(value []*PassportElementError) *SetPassportDataErrorsMethod {
	m.Errors = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetPassportDataErrorsMethod) Send(client *Client) (res *bool, err error) {
	return res, client.request("setPassportDataErrors", m, &res)
}

// SendGameMethod represents Telegram Bot API method sendGame
// Use this method to send a game. On success, the sent Message is returned.
type SendGameMethod struct {
	ChatID                   int                   `json:"chat_id" required:"true"`
	GameShortName            string                `json:"game_short_name" required:"true"`
	DisableNotification      bool                  `json:"disable_notification"`
	ReplyToMessageID         int                   `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool                  `json:"allow_sending_without_reply"`
	ReplyMarkup              *InlineKeyboardMarkup `json:"reply_markup"`
}

// SendGame creates new Telegram Bot API method sendGame
// Use this method to send a game. On success, the sent Message is returned.
func SendGame(chatID int, gameShortName string) *SendGameMethod {
	return &SendGameMethod{
		ChatID:        chatID,
		GameShortName: gameShortName,
	}
}

// SetChatID - Unique identifier for the target chat
func (m *SendGameMethod) SetChatID(value int) *SendGameMethod {
	m.ChatID = value
	return m
}

// SetGameShortName - Short name of the game, serves as the unique identifier for the game. Set up your games via Botfather.
func (m *SendGameMethod) SetGameShortName(value string) *SendGameMethod {
	m.GameShortName = value
	return m
}

// SetDisableNotification - Sends the message silently. Users will receive a notification with no sound.
func (m *SendGameMethod) SetDisableNotification(value bool) *SendGameMethod {
	m.DisableNotification = value
	return m
}

// SetReplyToMessageID - If the message is a reply, ID of the original message
func (m *SendGameMethod) SetReplyToMessageID(value int) *SendGameMethod {
	m.ReplyToMessageID = value
	return m
}

// SetAllowSendingWithoutReply - Pass True, if the message should be sent even if the specified replied-to message is not found
func (m *SendGameMethod) SetAllowSendingWithoutReply(value bool) *SendGameMethod {
	m.AllowSendingWithoutReply = value
	return m
}

// SetReplyMarkup - A JSON-serialized object for an inline keyboard. If empty, one 'Play game_title' button will be shown. If not empty, the first button must launch the game.
func (m *SendGameMethod) SetReplyMarkup(value *InlineKeyboardMarkup) *SendGameMethod {
	m.ReplyMarkup = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SendGameMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("sendGame", m, &res)
}

// SetGameScoreMethod represents Telegram Bot API method setGameScore
// Use this method to set the score of the specified user in a game. On success, if the message was sent by the bot, returns the edited Message, otherwise returns True. Returns an error, if the new score is not greater than the user's current score in the chat and force is False.
type SetGameScoreMethod struct {
	UserID             int    `json:"user_id" required:"true"`
	Score              int    `json:"score" required:"true"`
	Force              bool   `json:"force"`
	DisableEditMessage bool   `json:"disable_edit_message"`
	ChatID             int    `json:"chat_id"`
	MessageID          int    `json:"message_id"`
	InlineMessageID    string `json:"inline_message_id"`
}

// SetGameScore creates new Telegram Bot API method setGameScore
// Use this method to set the score of the specified user in a game. On success, if the message was sent by the bot, returns the edited Message, otherwise returns True. Returns an error, if the new score is not greater than the user's current score in the chat and force is False.
func SetGameScore(userID int, score int) *SetGameScoreMethod {
	return &SetGameScoreMethod{
		UserID: userID,
		Score:  score,
	}
}

// SetUserID - User identifier
func (m *SetGameScoreMethod) SetUserID(value int) *SetGameScoreMethod {
	m.UserID = value
	return m
}

// SetScore - New score, must be non-negative
func (m *SetGameScoreMethod) SetScore(value int) *SetGameScoreMethod {
	m.Score = value
	return m
}

// SetForce - Pass True, if the high score is allowed to decrease. This can be useful when fixing mistakes or banning cheaters
func (m *SetGameScoreMethod) SetForce(value bool) *SetGameScoreMethod {
	m.Force = value
	return m
}

// SetDisableEditMessage - Pass True, if the game message should not be automatically edited to include the current scoreboard
func (m *SetGameScoreMethod) SetDisableEditMessage(value bool) *SetGameScoreMethod {
	m.DisableEditMessage = value
	return m
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat
func (m *SetGameScoreMethod) SetChatID(value int) *SetGameScoreMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the sent message
func (m *SetGameScoreMethod) SetMessageID(value int) *SetGameScoreMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *SetGameScoreMethod) SetInlineMessageID(value string) *SetGameScoreMethod {
	m.InlineMessageID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *SetGameScoreMethod) Send(client *Client) (res *Message, err error) {
	return res, client.request("setGameScore", m, &res)
}

// GetGameHighScoresMethod represents Telegram Bot API method getGameHighScores
// Use this method to get data for high score tables. Will return the score of the specified user and several of their neighbors in a game. On success, returns an Array of GameHighScore objects.
type GetGameHighScoresMethod struct {
	UserID          int    `json:"user_id" required:"true"`
	ChatID          int    `json:"chat_id"`
	MessageID       int    `json:"message_id"`
	InlineMessageID string `json:"inline_message_id"`
}

// GetGameHighScores creates new Telegram Bot API method getGameHighScores
// Use this method to get data for high score tables. Will return the score of the specified user and several of their neighbors in a game. On success, returns an Array of GameHighScore objects.
func GetGameHighScores(userID int) *GetGameHighScoresMethod {
	return &GetGameHighScoresMethod{
		UserID: userID,
	}
}

// SetUserID - Target user id
func (m *GetGameHighScoresMethod) SetUserID(value int) *GetGameHighScoresMethod {
	m.UserID = value
	return m
}

// SetChatID - Required if inline_message_id is not specified. Unique identifier for the target chat
func (m *GetGameHighScoresMethod) SetChatID(value int) *GetGameHighScoresMethod {
	m.ChatID = value
	return m
}

// SetMessageID - Required if inline_message_id is not specified. Identifier of the sent message
func (m *GetGameHighScoresMethod) SetMessageID(value int) *GetGameHighScoresMethod {
	m.MessageID = value
	return m
}

// SetInlineMessageID - Required if chat_id and message_id are not specified. Identifier of the inline message
func (m *GetGameHighScoresMethod) SetInlineMessageID(value string) *GetGameHighScoresMethod {
	m.InlineMessageID = value
	return m
}

// Send is used to send request using Telegram Bot Client
func (m *GetGameHighScoresMethod) Send(client *Client) (res []*GameHighScore, err error) {
	return res, client.request("getGameHighScores", m, &res)
}
