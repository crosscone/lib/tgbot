package tg

import (
	"time"
)

// Poll starts polling incoming updates
func (c *Client) Poll(method *GetUpdatesMethod) error {
	_, err := GetMe().Send(c)
	if err != nil {
		return err
	}

	offset := method.Offset

	for {
		updates, _ := method.SetOffset(offset).Send(c)
		for _, update := range updates {
			c.processUpdate(update)
			offset = update.UpdateID + 1
		}

		time.Sleep(time.Microsecond * 500)
	}
}
