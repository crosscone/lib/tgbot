package tg

import (
	"strconv"
	"strings"
)

// Parse modes
const (
	ParseModeMarkdown   = "Markdown"
	ParseModeMarkdownV2 = "MarkdownV2"
	ParseModeHTML       = "HTML"
)

// EscapeMarkdown returns escaped Markdown text
func EscapeMarkdown(text string) string {
	var toEscape = "_*`["
	var sb strings.Builder

	for _, ch := range text {
		if strings.ContainsRune(toEscape, ch) {
			sb.WriteRune('\\')
		}

		sb.WriteRune(ch)
	}

	return sb.String()
}

// EscapeMarkdownV2 returns escaped MarkdownV2 text
func EscapeMarkdownV2(text string) string {
	var toEscape = "_*[]()~`>#+-=|{}.!\\"
	var sb strings.Builder

	for _, ch := range text {
		if strings.ContainsRune(toEscape, ch) {
			sb.WriteRune('\\')
		}

		sb.WriteRune(ch)
	}

	return sb.String()
}

// EscapeHTML returns escaped HTML text
func EscapeHTML(text string) string {
	text = strings.ReplaceAll(text, "<", "&lt;")
	text = strings.ReplaceAll(text, ">", "&gt;")
	text = strings.ReplaceAll(text, "&", "&amp;")
	text = strings.ReplaceAll(text, "\"", "&quot;")

	return text
}

// ChatIDType represents chat id
type ChatIDType string

// ChatID is dummy infterface method
func (t ChatIDType) ChatID() {}

// ChatIDNumber represents chat id
func ChatIDNumber(number int64) ChatID {
	return ChatIDType(strconv.FormatInt(number, 10))
}

// ChatIDUsername represents chat id
func ChatIDUsername(username string) ChatID {
	return ChatIDType(username)
}
