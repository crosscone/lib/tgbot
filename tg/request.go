package tg

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"reflect"
)

const baseURL = "https://api.telegram.org/bot"

type apiResponse struct {
	OK          bool            `json:"ok"`
	Result      json.RawMessage `json:"result"`
	Description string          `json:"description"`
}

func (c *Client) request(method string, req interface{}, resp interface{}) error {
	reader, contentType, err := makeRequestBuffer(req)
	if err != nil {
		return err
	}

	requestURL := fmt.Sprintf("%s%s/%s", baseURL, c.token, method)
	request, err := http.NewRequest("POST", requestURL, reader)
	if err != nil {
		return err
	}

	request.Header.Add("Content-Type", contentType)

	response, err := c.httpClient.Do(request)
	if err != nil {
		return err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}

	return readBody(body, &resp)
}

func readBody(body []byte, resp interface{}) error {
	var apiResponse = apiResponse{}
	err := json.Unmarshal(body, &apiResponse)
	if err != nil {
		return err
	}

	if !apiResponse.OK {
		return errors.New(apiResponse.Description)
	}

	return json.Unmarshal(apiResponse.Result, resp)
}

func makeRequestBuffer(req interface{}) (reader io.Reader, contentType string, err error) {
	var requestBuffer bytes.Buffer
	writer := multipart.NewWriter(&requestBuffer)
	defer writer.Close()

	formFields := []*formField{}

	reqValue := reflect.Indirect(reflect.ValueOf(req))
	reqType := reflect.TypeOf(reqValue.Interface())
	for i := 0; i < reqValue.NumField(); i++ {
		name, ok := reqType.Field(i).Tag.Lookup("json")
		if !ok {
			continue
		}

		_, isRequired := reqType.Field(i).Tag.Lookup("required")
		reqField := reqValue.Field(i)

		if !isRequired && reqField.IsZero() {
			continue
		}

		if reflect.ValueOf(reqField.Interface()).IsZero() {
			continue
		}

		reqFieldValue := reflect.Indirect(reflect.ValueOf(reqField.Interface())).Interface()

		if reqField.Kind() == reflect.Struct {
			formFields = append(formFields, newStructField(name, reqFieldValue))
			continue
		}

		field, err := makeField(name, reqFieldValue)
		if err != nil {
			continue
		}

		formFields = append(formFields, field)
	}

	if len(formFields) == 0 {
		return nil, "", nil
	}

	for _, field := range formFields {
		formWriter, _ := writer.CreateFormField(field.name)
		formWriter.Write(field.data)
	}

	return &requestBuffer, writer.FormDataContentType(), nil
}
