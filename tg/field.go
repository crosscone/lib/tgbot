package tg

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
)

type formField struct {
	name     string
	data     []byte
	filename string
}

func newBytesField(name string, value []byte) *formField {
	return &formField{name: name, data: value}
}

func newStringField(name string, value string) *formField {
	return newBytesField(name, []byte(value))
}

func newIntField(name string, value int) *formField {
	return newStringField(name, strconv.Itoa(value))
}

func newInt64Field(name string, value int64) *formField {
	return newStringField(name, strconv.FormatInt(value, 10))
}

func newFloat64Field(name string, value float64) *formField {
	return newStringField(name, fmt.Sprintf("%f", value))
}

func newBoolField(name string, value bool) *formField {
	if value {
		return newStringField(name, "true")
	}

	return newStringField(name, "false")
}

func newStructField(name string, value interface{}) *formField {
	jsonValue, _ := json.Marshal(value)

	return newBytesField(name, jsonValue)
}

func newChatIDField(name string, value ChatID) *formField {
	return newStringField(name, reflect.Indirect(reflect.ValueOf(value)).String())
}

func makeField(name string, data interface{}) (*formField, error) {
	switch v := data.(type) {
	case bool:
		return newBoolField(name, v), nil
	case int:
		return newIntField(name, v), nil
	case int64:
		return newInt64Field(name, v), nil
	case float64:
		return newFloat64Field(name, v), nil
	case string:
		return newStringField(name, v), nil
	case ChatID:
		return newChatIDField(name, v), nil
	}

	return newStructField(name, data), nil
}
