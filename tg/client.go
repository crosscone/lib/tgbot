package tg

import (
	"net/http"
	"sync"
)

// Client represents Telegram Bot client
type Client struct {
	token string

	httpClient *http.Client
	mtx        *sync.Mutex

	onUpdate func(*Update)
}

// New creates new Telegram Bot client
func New(token string) *Client {
	client := &Client{
		httpClient: new(http.Client),
		mtx:        new(sync.Mutex),
		token:      token,
	}

	return client
}

// OnUpdate sets callback which will receive pending updates
func (c *Client) OnUpdate(callback func(*Update)) {
	c.onUpdate = callback
}

func (c *Client) processUpdate(update *Update) {
	if c.onUpdate != nil {
		c.onUpdate(update)
	}
}
