# TGBot - Telegram Bot API Client

[![GoDoc](https://godoc.org/gitlab.com/crosscone//libtgbot/tg?status.svg)](https://godoc.org/gitlab.com/crosscone/lib/tgbot/tg)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/crosscone/lib/tgbot)](https://goreportcard.com/report/gitlab.com/crosscone/lib/tgbot)

Library using which you can easily create Telegram Bots